from pyramid.config import Configurator
from .base import Base


class Python(Base):

    LINES = []

    def __init__(self, request):
        Base.__init__(self, request)

    def get_facets(self):
        self.filtered = []
        for line in self.LINES:
            filter_out = False
            for key, value in self.query.fqs.items():
                if not line[key] == value:
                    filter_out = True
            if not filter_out:
                self.filtered.append(line)

    def facet_results(self, request):
        facets = {}
        for facet_field in self.model['facet_fields']:
            facets[facet_field] = []
            count = {}
            for line in self.filtered:
                if line[facet_field] in count:
                    count[line[facet_field]] += 1
                else:
                    count[line[facet_field]] = 1
            for key, value in count.items():
                facets[facet_field].append({'key': key, 'count': value})
            facets[facet_field].sort(reverse=True, key=lambda a: a['count'])  # nopep8

        facet_fields = self.model['facet_fields'][:]
        for facet_field in facet_fields:
            if facets[facet_field] == []:
                del facets[facet_field]
                facet_fields.remove(facet_field)
        self.result.update({'facet_fields': facet_fields,
                            'facets': facets})

    def get_docs(self):
        pass

    def docs_results(self, request):
        lines = []
        for line in self.filtered:
            lines.append(line)
        num_found = len(lines)
        batch_size = self.result['batch_size']
        self.result['batch'] = self.get_batch(num_found, batch_size)
        response = {'numFound': num_found, 'docs': lines[:batch_size]}
        self.result['table'] = {'response': response}

    def driller(self):
        pass

    def get_driller(self):
        pass

    def driller_results(self, driller):
        pass


class PythonFromFile(Python):

    def __init__(self, request, file_path=None, parser_dotted=None):
        Base.__init__(self, request)
        if len(self.LINES) != 0:
            return
        if file_path is None:
            file_path = request.registry.settings.get('like.solr.python.csv')
        if parser_dotted is None:
            parser_dotted = request.registry.settings.get('like.solr.python.parser')  # nopep8
        config = Configurator()
        parser = config.maybe_dotted(parser_dotted)
        self.LINES = parser(file_path)
