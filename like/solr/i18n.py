import types
import simplejson


def translate(request, value):
    if isinstance(value, dict):
        try:
            return value[request.locale_name]
        except:
            return value
    value = value.strip()
    if value.startswith('{') and value.endswith('}'):
        try:
            return simplejson.loads(value)[request.locale_name]
        except:
            return value
    else:
        return value
