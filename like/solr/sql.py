import re
from .base import Base
import sqlite3
from .dashboard.main import render_driller
#from substancedsite.models import DBSession


class Sql(Base):

    DB = None

    def __init__(self, request):
        Base.__init__(self, request)

    def get_facets(self):
        for line in self.LINES:
            filter_out = False
            for fq in self.request.params.getall('fq'):
                if ':' in fq:
                    key, value = fq.split(':', 1)
                    value = value[1:-1]
                    if key in line:
                        if not line[key] == value:
                            filter_out = True
            if not filter_out:
                yield line

    def facet_results(self, lines):
        if lines is None:
            self.result.update({'facet_fields': [],
                                'facets': {}})
            return
        lines = [l for l in lines]
        facets = {}
        for facet_field in self.model['all_fields']:
            facets[facet_field] = []
            count = {}
            for line in lines:
                if facet_field in line:
                    if line[facet_field] in count:
                        count[line[facet_field]] += 1
                    else:
                        count[line[facet_field]] = 1
            for key, value in count.items():
                facets[facet_field].append({'key': key, 'count': value})
            facets[facet_field].sort(reverse=True, key=lambda a: a['count'])

        facet_fields = self.model['facet_fields'][:]
        for facet_field in facet_fields:
            if facet_field in facets:
                if facets[facet_field] == []:
                    del facets[facet_field]
                    facet_fields.remove(facet_field)
        self.result['facet_fields'] = facet_fields
        self.result['facets'] = facets
        return lines

    def get_doc(self):
        for fq in self.query.params.getall('fq'):
            if fq.split(':')[0] == self.model['id_key']:
                key, value = fq.split(':', 1)
                if value.startswith('"') and value.endswith('"'):
                    value = value[1:-1]
                    for line in self.LINES:
                        if line[key] == value:
                            return line

    def doc_results(self, doc):
        if doc is None:
            response = {'numFound': 0,
                        'doc': None}
        else:
            response = {'numFound': 1,
                        'doc': doc}
        return response

    def docs_results(self, request):
        lines = []
        for line in self.filtered:
            lines.append(line)
        numFound = len(lines)
        if 'sort' in self.query.params:
            sort_on = ' '.join(self.query.params['sort'].split()[:-1])
            sort_direction = self.query.params['sort'].split()[-1] == 'desc'
            lines.sort(reverse=sort_direction, key=lambda a: a.get(sort_on, '').lower())  # nopep8
        self.result['batch'] = self.get_batch(numFound, self.result['batch_size'])  # nopep8
        start = self.result['batch'].start
        end = self.result['batch'].end
        response = {'numFound': numFound,
                    'docs': lines[start - 1: end]}
        self.result['table'] = {'response': response}

    def driller(self):
        pass

    def get_driller(self):
        return True

    def driller_results(self, driller, lines, labels):
        dims = []
        for item in self.request.params.getall('fq'):
            if ':' in item:
                continue
            if item.startswith('"') and item.endswith('"'):
                dims.append(item[1:-1])
        if len(dims) == 0:
            return

        self.DB = sqlite3.connect(':memory:')
        # self.DB.row_factory = sqlite3.Row
        cursor = self.DB.cursor()

        sql = u'create table %s ("%s")' % (self.table_name, '","'.join(dims))
        cursor.execute(sql)

        names = ','.join(list('?' * len(dims)))
        sql = u"""insert into %s values (%s)""" % (self.table_name, names)

        for row in lines:
            try:
                cursor.execute(sql, [row[dim] for dim in dims])
            except:
                # Line can not be added to the database
                raise

        if len(dims) == 1:
            rows = '\n'.join(dims)
            cols = ''
        else:
            rows = '\n'.join(dims[:-1])
            cols = dims[-1]
        self.result['driller'] = render_driller(rows, cols, 'id', self.DB, self.request, self.query, labels, self.table_name)  # nopep8

    def get_all_facet_counts(self, facet_field):
        class Result:
            text = '{"facet_counts":{"facet_fields":{"scientificName":[]}}}'
        return Result()

    def get_simple_facet_count(self):
        dims = []
        for item in self.request.params.getall('fq'):
            if ':' in item:
                continue
            if item.startswith('"') and item.endswith('"'):
                dims.append(item[1:-1])
        if len(dims) == 0:
            return None
        facet_field = dims[0]
        count = {}
        for line in self.filtered:
            if line[facet_field] in count:
                count[line[facet_field]] += 1
            else:
                count[line[facet_field]] = 1
        facet_counts = {facet_field: []}
        for key, value in count.items():
            facet_counts[facet_field].append({'key': key, 'count': value})
        facet_counts[facet_field].sort(reverse=True, key=lambda a: a['count'])  # nopep8
        return facet_counts

    def simple_facet_count_results(self, facet_counts):
        facet_field = list(facet_counts.keys())[0]
        counts = facet_counts[facet_field]
        self.result['batch'] = self.get_batch(len(counts),
                                              self.result['batch_size'] * 4)
        #print len(counts), self.result['batch_size'] * 4
        self.result['driller'] = {'facet_field': facet_field,
                                  'simple_facet_count': counts}

class SqliteFromCsv(Sql):

    def __init__(self, request, headers, reader, delimiter, id_key, edits=[]):

        Sql.__init__(self, request)

        self.request = request
        self.DB = sqlite3.connect(':memory:')
        # self.DB.row_factory = sqlite3.Row
        cursor = self.DB.cursor()
        table_name = 'specimen'

        result = [line for line in reader]

        # When we add new lines, we need to know the current number of existing lines
        new_line_number = len(result)

        search_term = request.params.get('search_term', None)
        if search_term is not None:
            prog = re.compile(search_term)

        if len(result) > 0:
            self.NAMES = headers
        else:
            return

        # If this is a single line, reduce the result
        self.initial_values = {}
        id_index = self.NAMES.index(id_key)
        if id_key in self.query.fqs:
            # When a specific id_key is given, the result must be a single line
            id_value = self.query.fqs[id_key]
            # Reduce the result to one line
            id_result = []
            for row in result:
                if row[id_index] == id_value:
                    id_result = [row]
                    break
            self.initial_values = {}
            if id_result == []:
                for header in headers:
                    self.initial_values[header] = ''
            else:
                index = 0
                for header in headers:
                    self.initial_values[header] = id_result[0][index]
                    index = index + 1
            result = id_result

        if len(edits) > 0:
            # Count number of new lines
            changes = {}

            # Collect all changes in a dictionary by id
            changes = {}
            for edit in edits:
                if 'action' in edit:
                    if edit['action'] == 'add_line':
                        add_line = [u''] * len(self.NAMES)
                        add_line[id_index] = str(new_line_number)
                        # If this is a single line, only add the matching line
                        if id_key in self.query.fqs:
                            if self.query.fqs[id_key] == add_line[id_index]:
                                result.append(add_line)
                        else:
                            result.append(add_line)
                        new_line_number += 1
                else:
                    edit['index'] = self.NAMES.index(edit['dim_key'])
                    if edit['id_value'] in changes:
                        changes[id_value].append(edit)
                    else:
                        changes[edit['id_value']] = [edit]

            # Patch result
            for row in result:
                if row[id_index] in changes:
                    for edit in changes[row[id_index]]:
                        row[edit['index']] = edit['dim_value']

        sql = u'create table %s ("%s")' % (table_name, '","'.join(headers))
        cursor.execute(sql)

        names = ','.join(list('?' * len(headers)))
        sql = u"""insert into %s values (%s)""" % (table_name, names)

        for row in result:
            filter_out = False
            search_found = False
            for fq in self.request.params.getall('fq'):
                if ':' in fq:
                    key, value = fq.split(':', 1)
                    value = value[1:-1]
                    if key in self.NAMES:
                        if not row[self.NAMES.index(key)] == value:
                            filter_out = True
                            break
            if search_term is not None:
                for cell in row:
                    if not prog.search(cell) is None:
                        search_found = True
                        break
            if search_term is not None:
                if not search_found:
                    filter_out = True
            if not filter_out:
                try:
                    cursor.execute(sql, row)
                except:
                    # Line can not be added to the database
                    raise

    def get_facets(self):
        cursor = self.DB.cursor()
        rows = cursor.execute("""select * from specimen""")
        self.LINES = [dict(zip(self.NAMES, r)) for r in rows]
        Sql.get_facets(self)

    def get_doc(self):
        cursor = self.DB.cursor()
        rows = cursor.execute("""select * from specimen""")
        self.LINES = [dict(zip(self.NAMES, r)) for r in rows]
        Sql.get_doc(self)




class SqliteFromJSON(Sql):

    def __init__(self, request, json, headers, id_key, edits=[], history={}):
        """
        Example history setting:
        
         {'input_fields': ['input1', 'input2']
          'output_fields': ['output1', 'output2', 'output3'],
          'has_edit_at': '2017-09-10T23:54:39.425424 02:00'}
        """
        Sql.__init__(self, request)

        self.request = request
        self.DB = sqlite3.connect(':memory:')
        # self.DB.row_factory = sqlite3.Row
        cursor = self.DB.cursor()
        table_name = 'specimen'

        search_term = request.params.get('search_term', None)
        if search_term is not None:
            prog = re.compile(search_term)

        self.NAMES = []
        if len(json) > 0 or len(edits) > 0:
            self.NAMES = headers

        if len(edits) > 0:
            # Count number of new lines
            changes = {}

            # Initialize json with ids
            new_line_number = 0
            json = []
            for edit in edits:
                if 'id_value' not in edit:
                    continue
                value = {id_key: str(edit['id_value'])}
                if value not in json:
                    json.append(value)
                    # When we add new lines, we need to know the current number of existing lines
                    new_line_number = max(new_line_number, int(edit['id_value']))
            new_line_number += 1

            # Collect all changes in a dictionary by id
            changes = {}
            show_history_for_id = set()
            for edit in edits:
                if 'action' in edit:
                    if edit['action'] == 'add_line':
                        add_line = {}
                        add_line[id_index] = str(new_line_number)
                        # If this is a single line, only add the matching line
                        if id_key in self.query.fqs:
                            if self.query.fqs[id_key] == add_line[id_index]:
                                json.append(add_line)
                        else:
                            json.append(add_line)
                        new_line_number += 1
                # Add the edit to the changes
                edit['index'] = self.NAMES.index(edit['dim_key'])
                if edit['id_value'] in changes:
                    changes[edit['id_value']].append(edit)
                else:
                    changes[edit['id_value']] = [edit]
                # Keep a record of what ids to show in the history
                # If there is an edit at a specific timestamp then this indicates that this
                # id should be shown in the history
                if 'has_edit_at' in history:
                    if edit['timestamp'] == history['has_edit_at'].replace(' ', '+'):
                        if edit['dim_key'] in history['input_fields']:
                            show_history_for_id.add(edit['id_value'])
                else:
                    # Business as usual
                    show_history_for_id.add(edit['id_value'])

            self.history = {}
            # Patch result
            for row in json:
                for name in self.NAMES:
                    if name not in row:
                        row[name] = ""
                if row[id_key] in changes:
                    for edit in changes[row[id_key]]:
                        row[edit['dim_key']] = edit['dim_value']
                        # Ignore ids for which no history should be shown
                        if row[id_key] in show_history_for_id:                        
                            history_key = (row[id_key], edit['dim_key'])
                            if history_key in self.history:
                                if self.history[history_key][0]['dim_value'].strip() == edit['dim_value'].strip():
                                    # noop
                                    pass
                                else:
                                    self.history[history_key].insert(0, edit)
                            else:
                                self.history[history_key] = [edit]

        sql = u'create table %s ("%s")' % (table_name, '","'.join(self.NAMES))
        cursor.execute(sql)

        names = ','.join(list('?' * len(self.NAMES)))
        sql = u"""insert into %s values (%s)""" % (table_name, names)

        json.sort(reverse=False, key=lambda a: int(a[id_key]))

        for row in json:
            filter_out = False
            search_found = False
            for fq in self.request.params.getall('fq'):
                if ':' in fq:
                    key, value = fq.split(':', 1)
                    value = value[1:-1]
                    if key in self.NAMES:
                        if not row[key] == value:
                            filter_out = True
                            break
            if search_term is not None:
                for cell in row:
                    if not prog.search(cell) is None:
                        search_found = True
                        break
            if search_term is not None:
                if not search_found:
                    filter_out = True
            if not filter_out:
                try:
                    cursor.execute(sql, [row[i] for i in self.NAMES])
                except:
                    # Line can not be added to the database
                    raise

    def get_facets(self):
        cursor = self.DB.cursor()
        rows = cursor.execute('''select * from specimen''')
        self.LINES = [dict(zip(self.NAMES, r)) for r in rows]
        Sql.get_facets(self)

    def get_doc(self):
        cursor = self.DB.cursor()
        rows = cursor.execute('''select * from specimen''')
        self.LINES = [dict(zip(self.NAMES, r)) for r in rows]
        Sql.get_doc(self)


def search_filter(request, names, lines):
    search_term = request.params.get('search_term', None)
    if search_term is not None:
        prog = re.compile(search_term)

    for line in lines:
        filter_out = False
        search_found = False
        for fq in request.params.getall('fq'):
            if ':' in fq:
                key, value = fq.split(':', 1)
                value = value[1:-1]
                if key in names:
                    if not line[key] == value:
                        filter_out = True
                        break
        if search_term is not None:
            for cell in line.values():
                if not prog.search(cell) is None:
                    search_found = True
                    break
        if search_term is not None:
            if not search_found:
                filter_out = True

        if not filter_out:
            yield line

    raise StopIteration



class SqliteFromDB(Sql):

    def __init__(self, request, headers, id_key, edits=[], history={}):
        Sql.__init__(self, request)
        self.DB = request.db_session
        self.NAMES = headers
        self.request = request
        self.headers = headers
        self.id_key = id_key
        self.edits = edits
        self.history = history
        self.table_name = self.request.path.split("/")[-1] or 'hubs'

    def get_facets(self):
        if self.id_key in self.query.fqs:
            return
        sql_query = '''select "%s" from %s''' % ('","'.join(self.NAMES), self.table_name)
        return search_filter(self.request, self.NAMES, self.facet_filter(self.request, self.fetch_rows(self.request.db_session.execute(sql_query))))

    def get_doc(self):
        if self.id_key in self.query.fqs:
            id_value = self.query.fqs[self.id_key]
            sql_query = '''select "%s" from %s where "%s" = "%s"''' % ('","'.join(self.NAMES), self.table_name, self.id_key, id_value)
            rows = self.request.db_session.execute(sql_query)
            return [i for i in self.fetch_rows(rows)][0]

    def fetch_rows(self, rows):
        for row in rows:
            doc = dict(zip(self.NAMES, row))
            yield doc
        raise StopIteration

    def facet_filter(self, request, lines):
        for line in lines:
            filter_out = False
            for fq in request.params.getall('fq'):
                if ':' in fq:
                    key, value = fq.split(':', 1)
                    value = value[1:-1]
                    if key in line:
                        if not line[key] == value:
                            filter_out = True
            if not filter_out:
                yield line
        raise StopIteration

    def docs_results(self, lines):
        if lines is None:
            response = {'numFound': 0,
                        'docs': []}
            return response
        else:
            numFound = len(lines)
        if 'sort' in self.query.params:
            sort_on = ' '.join(self.query.params['sort'].split()[:-1])
            sort_direction = self.query.params['sort'].split()[-1] == 'desc'
            lines.sort(reverse=sort_direction, key=lambda a: a.get(sort_on, '').lower())  # nopep8
        self.result['batch'] = self.get_batch(numFound, self.result['batch_size'])  # nopep8
        start = self.result['batch'].start
        end = self.result['batch'].end
        response = {'numFound': numFound,
                    'docs': lines[start - 1: end]}
        self.result['table'] = {'response': response}


    def get_simple_facet_count(self, filtered):
        dims = []
        for item in self.request.params.getall('fq'):
            if ':' in item:
                continue
            if item.startswith('"') and item.endswith('"'):
                dims.append(item[1:-1])
        if len(dims) == 0:
            return None
        facet_field = dims[0]
        count = {}
        for line in filtered:
            if line[facet_field] in count:
                count[line[facet_field]] += 1
            else:
                count[line[facet_field]] = 1
        facet_counts = {facet_field: []}
        for key, value in count.items():
            facet_counts[facet_field].append({'key': key, 'count': value})
        facet_counts[facet_field].sort(reverse=True, key=lambda a: a['count'])  # nopep8
        return facet_counts

    def simple_facet_count_results(self, facet_counts):
        facet_field = list(facet_counts.keys())[0]
        counts = facet_counts[facet_field]
        self.result['batch'] = self.get_batch(len(counts),
                                              self.result['batch_size'] * 4)
        #print len(counts), self.result['batch_size'] * 4
        self.result['driller'] = {'facet_field': facet_field,
                                  'simple_facet_count': counts}

