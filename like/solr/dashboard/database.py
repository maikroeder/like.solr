import os
import sqlite3
import csv


def write_sqlite3_table(cursor, csv_file_path, table_name, columns):
    csv_file = open(csv_file_path, 'r')
    lines = [line for line in csv.DictReader(csv_file, delimiter='\t')]
    types = []
    for column in columns:
        types.append("%s text" % column)
    cursor.execute('''create table %s (%s)''' % (table_name, ",".join(types)))
    names = ','.join(list('?' * len(columns)))
    sql = """insert into %s values (%s)""" % (table_name, names)
    for line in lines:
        row = [line[key] for key in columns]
        cursor.execute(sql, row)


def produce_sqlite3_database():
    output = "binhum.db"
    if os.path.exists(output):
        os.remove(output)
    connection = sqlite3.connect(output)
    cursor = connection.cursor()
    columns = "associationtype,collectioncode,collectorname,coordinateErrorDistanceInMeters,country,created_when,epithet,fk_tripleidstoreid1,fk_tripleidstoreid2,fullScientificName,gatheringdate,genus,geo_locality,higherrank,highertaxon,id,identifiername,institutioncode,last_harvested,latitude,locality,longitude,mm_format,mm_url,rawidentificationid,scientificName,score,source_url,typestatus,unitid,verificationNotes".split(',')  # nopep8
    write_sqlite3_table(cursor, 'solr.csv', 'specimen', columns)
    connection.commit()
    cursor.close()

if __name__ == "__main__":
    produce_sqlite3_database()
