"""
Render a dashboard as HTML given the context and a cube
"""
from io import StringIO
try:
    from urllib.parse import quote
except ImportError:
    from urllib import quote
    

class Table:
    """
    Renders the dashboard table.
    """

    def __init__(self, cubes, request, query, labels):
        """
        Store the contect and the cube.
        """
        self.cubes = cubes
        self.dimensions = cubes['specimen'].context['dimensions']
        self.id_key = cubes['specimen'].context['id_key']
        self.request = request
        self.query = query
        self.labels = labels

        params = []
        for key, value in self.query.params.items():
            if key == 'tab':
                continue
            if key == 'fq':
                if ':' not in value:
                    continue
            params.append(key + '=' + quote(value))
        self.base_url = '?' + '&'.join(params)

    def render(self):
        """
        Render the HTML page parts.
        """
        output = StringIO()
        self.top(output)
        self.rows_header(output)
        self.columns_header(output)
        self.rows(output)
        self.bottom(output)
        html = output.getvalue()
        output.close()
        return html

    def top(self, output):
        """
        Render the top of the table.
        """
        output.write(u'''<table class="table table-bordered table-responsive table-condensed table-hover row-clickable"><tbody>''')  # nopep8
        if len(self.cubes['specimen'].get_cols()) == 1:
            for col in self.cubes['specimen'].get_cols():
                output.write(u"""<tr>\n""")
                self.space_above_rows(output)
                self.space_above_cols(output)
                output.write(u"</tr>\n")
        if len(self.cubes['specimen'].get_cols()) > 1:
            for col in self.cubes['specimen'].get_cols()[:-1]:
                output.write(u"""<tr>\n""")
                self.space_above_rows(output)
                self.columns_tree(output, col)
                output.write(u"</tr>\n")

    def space_above_rows(self, output):
        """
        Render the space above the rows of the table.
        """
        num = len(self.cubes['specimen'].get_rows())
        output.write(u"""<th><div>&nbsp;</div></th>\n""" * num)

    def space_above_cols(self, output):
        """
        Render the space above the rows of the table.
        """
        num = len([i for i in self.cubes['specimen'].get_col_product()])
        html = u'<th colspan="%s"><div style="color: #7e0000;">%s</div></th>\n'
        dimension = self.cubes['specimen'].get_cols()[0]
        output.write(html % (num, self.labels.get(dimension)))

    def columns_tree(self, output, col):
        """
        Render the columns tree of the table.
        """
        col_index = self.cubes['specimen'].get_cols().index(col)
        for item in self.cubes['specimen'].get_col_product():
            template = u"""<th><div>%s</div></th>\n"""
            output.write(template % item[col_index])

    def rows_header(self, output):
        """
        Render the rows header of the table.
        """
        output.write(u"""<tr>\n""")
        for dimension in self.cubes['specimen'].get_rows():
            template = u"""<th><div style="color: #7e0000;">%s</div></th>\n"""
            output.write(template % self.labels.get(dimension))

    def columns_header(self, output):
        """
        Render the columns header of the table.
        """
        for item in self.cubes['specimen'].get_col_product():
            if item:
                output.write(u"""<th><div>%s</div></th>\n""" % item[-1])
            else:
                output.write(u"""<th><div></div></th>\n""")
        output.write(u"</tr>\n")

    def rows(self, output):
        """
        Render the rows of the table.
        """
        row_index = 0
        for row_value in self.cubes['specimen'].get_row_values():
            output.write(u"""<tr>\n""")
            self.row(output, row_value, row_index)
            row_index += 1
            output.write(u"</tr>\n")

    def row(self, output, row_value, row_index):
        """
        Render the row labels and values
        """
        self.row_labels(output, row_value)
        self.row_values(output, row_value, row_index)

    def row_labels(self, output, row_value):
        """
        Render the row labels
        """
        for index in range(0, len(self.cubes['specimen'].get_rows())):
            output.write(u"""<th>\n""")
            output.write(u"""<div>%s</div>\n""" % row_value[index])
            output.write(u"""</th>\n""")

    def row_values(self, output, row_value, row_index):
        """
        Render the row values
        """
        data = {'row_value': row_value,
                'row_index': row_index}
        col_index = 0
        for col_value in self.cubes['specimen'].get_col_product():
            data['col_index'] = col_index
            data['col_value'] = col_value
            self.cell(output, data)
            col_index += 1

    def cell(self, output, data):
        """
        Render the cell
        """
        key = tuple(data['row_value']) + data['col_value']
        if key in self.cubes['rows_and_cols'].get_row_values():
            output.write(u"""<td>\n<div>\n""")
            fqs = []
            i = 0
            for row in self.cubes['rows_and_cols'].get_rows():
                value = quote('"' + key[i] + '"')
                fqs.append(u'fq=%s:%s' % (row, value))
                i += 1
            i = 0
            for col in self.cubes['rows_and_cols'].get_cols():
                if col != self.id_key:
                    value = quote('"' + key[i] + '"')
                    fqs.append('fq=%s:"%s"' % (col, value))
                i += 1
            url = self.base_url + '&tab=dashboard&' + '&'.join(fqs)
            length = len(self.cubes['rows_and_cols'].mapping[key])
            output.write(u"""<a href="%s">%s</a>\n""" % (url, length))
            output.write(u"""</div>\n</td>\n""")
        else:
            html = (u"""<td><div>"""
                    u"""</div></td>\n""")
            output.write(html)

    def bottom(self, output):
        """
        Render bottom of the table and the html page.
        """
        output.write(u"</tbody></table>")

    def rnaseq_homepage_link(self, output, key, text):
        """
        Produce the homepage link
        """
        all_headers = []
        new_headers = []
        for header in self.cubes['files'].get_cols():
            if header not in self.dimensions:
                new_headers.append(header)
            all_headers.append(header)
        link = self.get_link(key, all_headers, text)
        output.write(link)
        return

    def get_link(self, key, all_headers, text):
        """
        Make a link to the homepage using the parameter information
        """
        replicate = self.cubes['files'].get_cell(key)[0]
        line = dict(zip(all_headers, replicate))
        parameters = []
        subset_parameters = self.cubes['files'].context['subset_parameters']
        for dim in subset_parameters:
            parameters.append(line[dim])
        base = u"http://rnaseq.crg.es/project/%s/" % line['project_id']
        params = u'-'.join(subset_parameters)
        values = u'-'.join(parameters)
        link = base + "%s/%s/tab/overview/" % (params, values)
        tag = u"""<a href="%s">%s</a>""" % (link, text)
        return tag
