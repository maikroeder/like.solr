"""
Prepare the context and the cube and then render the dashboard
"""
from .dashboard import Dashboard
from .cube import Cube


def get_key(line):
    """
    Get a key for the project_id and accession_id from the line
    """
    return (line['project_id'], line['accession_id'])


def get_dimensions(context):
    """
    Get the dimensions in a dict of rows and cols keys.
    """
    dimensions = {}
    for row in context['rows']:
        dimensions[row] = row
    for col in context['cols']:
        dimensions[col] = col
    return dimensions


def main(options, request, query, labels):
    """
    Put the configuration into a context dictionary and render the dashboard
    as HTML.

    Context setup example:

    rows: ['species', 'cell', 'rnaExtract', 'localization', 'label']
    cols: ['readType']
    filters: {}
    lines: <csv.DictReader instance at 0x10138a1b8>
    dimensions: {'localization': 'Localization',
                 'readType': 'Read type',
                 'label': 'Condition',
                 'cell': 'Cell type',
                 'rnaExtract': 'RNA extract',
                 'species': 'Species'}
    """

    cubes = {}

    context = {}
    context['rows'] = options['rows'].split('\n')
    if options['cols']:
        context['cols'] = options['cols'].split('\n')
    else:
        context['cols'] = []
    context['dbconn'] = options['dbconn']
    context['subset_parameters'] = options['subset_parameters'].split('\n')
    context['dimensions'] = get_dimensions(context)
    context['id_key'] = options['id_key']
    cubes['specimen'] = Cube(context, options['table_name'])

    context = {}
    if options['cols']:
        context['rows'] = options['rows'].split('\n') + options['cols'].split('\n')  # nopep8
    else:
        context['rows'] = options['rows'].split('\n')
    context['cols'] = [options['id_key']]
    context['subset_parameters'] = options['subset_parameters'].split('\n')
    context['dbconn'] = options['dbconn']
    context['dimensions'] = get_dimensions(context)
    cubes['rows_and_cols'] = Cube(context, options['table_name'])

    title = options.get('title', None)
    description = options.get('description', None)

    dashboard = Dashboard(cubes, title, description, request, query, labels)
    html = dashboard.render()

    return html


def render_driller(rows, cols, id_key, dbconn, request, query, labels, table_name):
    options = {'rows': rows,
               'cols': cols,
               'id_key': id_key,
               'subset_parameters': '',
               'dbconn': dbconn,
               'description': '',
               'output_file': 'dashboard.html',
               'table_name': table_name
               }
    return main(options, request, query, labels)
