""" Utility methods for the Substance D portal.
"""
import requests
from requests import RequestException
from requests import ConnectionError
from requests import HTTPError
from requests import URLRequired
from requests import TooManyRedirects
from requests.exceptions import Timeout
from simplejson import JSONDecodeError
import socket


def call_log_method(why, what, log, level):
    """ Call the log method with the error message

        why: For example 'Request timeout'
        what: For example 'Fetch facets'
        log: the log method
        level: the log level, for example, "critical", "error", "warning", "info", "debug", "notset"
    """
    if level not in ["critical", "error", "warning", "info", "debug", "notset"]:
        raise AttributeError
    if log is not None:
        log(why + ' during: ' + what, level)


def get_url(solr_url, what, log=None, auth=None):
    """ Use the requests module to get a URL from Solr.
        Returns the response object or None if the request failed.
        The log method needs to access a msg and level argument
    """
    response = None
    try:
        if auth is None:
            response = requests.get(solr_url, timeout=5)
        else:
            response = requests.get(solr_url, auth=auth, timeout=10)
    except (Timeout, socket.timeout):
        call_log_method('Request timeout', what, log, 'error')
    except ConnectionError:
        call_log_method('Connection error', what, log, 'error')
    except HTTPError:
        call_log_method('HTTP error', what, log, 'error')
    except (URLRequired, TooManyRedirects):
        call_log_method('Request error', what, log, 'error')
    except RequestException:
        call_log_method('Request exception error', what, log, 'error')
    if response is not None:
        if response.status_code != 200:
            why = 'Request failed with status code %s' % response.status_code
            call_log_method(why, what, log, 'error')
    return response


def get_json(response, what, log=None):
    """ Try getting JSON out of a response from a call to the requests module.
        Flash a message in case there is a problem.
    """
    if response is None:
        return None
    data = None
    try:
        data = response.json()
    except JSONDecodeError:
        pass
    if data is None:
        text = ''
        try:
            text = response.text
        except:
            pass
        message = "503 Service Unavailable"
        if message in text:
            call_log_method('Error: %s' % message, what, log, 'error')
        else:
            call_log_method('JSON error', what, log, 'error')
    return data
