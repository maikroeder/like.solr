import time
import simplejson
import datetime
import requests
import uuid
from pytz import timezone
try:
    from urllib.parse import urljoin
except ImportError:
    from urlparse import urljoin
from requests import ConnectionError

KEYWORDS = """charset
csv.separator
facet
facet.date
facet.date.end
facet.date.gap
facet.date.hardend
facet.date.include
facet.date.other
facet.date.start
facet.enum.cache.minDf
facet.field
facet.limit
facet.method
facet.mincount
facet.missing
facet.offset
facet.pivot
facet.prefix
facet.query
facet.range
facet.range.end
facet.range.gap
facet.range.hardend
facet.range.include
facet.range.other
facet.range.start
facet.sort
facet.threads
fl
fq
group.cache.percent
group.field
group.format
group.func
group.limit
group.main
group.ngroups
group.offset
group.query
group.sort
hl.alternateField
hl.boundaryScanner
hl.bs.chars
hl.bs.country
hl.bs.language
hl.bs.maxScan
hl.bs.type
hl.fl
hl.formatter
hl.fragListBuilder
hl.fragmenter
hl.fragmentsBuilder
hl.fragsize
hl.highlightMultiTerm
hl.maxAlternateFieldLength
hl.maxAnalyzedChars
hl.maxMultiValuedToExamine
hl.maxMultiValuedToMatch
hl.mergeContiguous
hl.preserveMulti
hl.q
hl.regex.maxAnalyzedChars
hl.regex.pattern
hl.regex.slop
hl.requireFieldMatch
hl.simple.post
hl.simple.pre
hl.snippets
hl.useFastVectorHighlighter
hl.usePhraseHighlighter
key
mlt.boost
mlt.count
mlt.fl
mlt.maxntp
mlt.maxqt
mlt.maxwl
mlt.mindf
mlt.mintf
mlt.minwl
mlt.qf
q
rows
sort
start
wt"""

# Code for converting utc to string:
# https://code.google.com/p/solrpy/source/browse/solr/core.py?name=

# -------------------------------------------------------------------
# Datetime extensions to parse/generate Solr date formats
# -------------------------------------------------------------------
# A UTC class, for parsing Solr's returned dates.


class UTC(datetime.tzinfo):
    """
    UTC timezone.
    """
    def utcoffset(self, dt):
        return datetime.timedelta(0)

    def tzname(self, dt):
        return "UTC"

    def dst(self, dt):
        return datetime.timedelta(0)


utc = UTC()


def utc_to_string(value):
    """
    Convert datetimes to the subset of ISO 8601 that Solr expects.
    """
    value = value.astimezone(utc).isoformat()
    if '+' in value:
        value = value.split('+')[0]
    value += 'Z'
    return value


class Performance():

    def __init__(self, performance_core_url):
        """
        The performance_core_url parameter should point to the Solr core that
        stores the performance logs, e.g.
        http://localhost:8983/solr/performance
        """
        self.target = urljoin(performance_core_url, 'update?commit=true')
        self.start_time = None
        self.group_date = utc_to_string(datetime.datetime.now(timezone('UTC')))
        self.group_time = time.time()
        self.log_count = 0
        self.payload = []

    def start(self):
        self.start_time = time.time()

    def stop(self, host, path_url, web_url, solr_url, params, action, status_code):
        self.payload = []
        if self.start_time is None:
            raise AttributeError
        headers = {'content-type': 'application/json'}
        self.log_count += 1
        self.payload.append({'UUID': str(uuid.uuid4()),
                             'PATH_URL': path_url,
                             'REQUEST': self.group_date,
                             'REQUEST_AGGREGATE': time.time() - self.group_time,  # nopep8
                             'SECONDS': time.time() - self.start_time,
                             'TIMESTAMP': utc_to_string(datetime.datetime.now(timezone('UTC'))),  # nopep8
                             'SOLR_URL': solr_url,
                             'WEB_URL': web_url,
                             'ACTION': action,
                             'STATUS_CODE': status_code})
        self.payload[0].update(params.dict_of_lists())
        self.start_time = None
        data = simplejson.dumps(self.payload, indent=4 * ' ', ensure_ascii=False)
        try:
            result = requests.post(self.target, data=data, headers=headers)
        except ConnectionError:
            raise
        if result.status_code != 200:
            raise AttributeError(result.content)
