import unicodecsv as csv
import types
import sys
from webob.multidict import MultiDict
try:
    from urllib.parse import quote
except ImportError:
    from urllib import quote
from pprint import pformat
from .base import Base
import sqlite3
from .dashboard.main import render_driller
from .performance import Performance
try:
    from urllib.parse import urljoin
except ImportError:
    from urlparse import urljoin
from .utils import get_url
from .utils import get_json


def join_params(params, inject=""):
    """
    Combine all parameters into a URL of the form
    ?a=1&b=2&c=3
    The inject parameter can be of the form
    a=1&b=2
    """
    if len(inject) > 0:
        query_items = [inject]
    else:
        query_items = []
    for key, value in params.items():
        if type(value) is list:
            for item in value:
                query_items.append(key + '=' + item)
        else:
            query_items.append(key + '=' + value)
    if len(query_items) > 0:
        return '?' + '&'.join(query_items)
    else:
        return ''


class Solr(Base):

    SERVER = None

    def __init__(self, request, server=None, auth=None, performance_logging=False):  # nopep8
        Base.__init__(self, request)
        if server is None:
            server = request.registry.settings['like.solr.server']
        self.SERVER = server
        self.auth = auth
        if performance_logging:
            self.performance = Performance(request.registry.settings.get('performance.log', None))  # nopep8
        else:
            self.performance = None

    def get_solr_url(self, command, params):
        return urljoin(self.SERVER, command + join_params(params, self.model['inject']))  # nopep8

    def get(self, command, params, what):
        self.__solr_url___ = self.get_solr_url(command, params)
        if self.performance is not None:
            self.performance.start()
        result = get_url(self.__solr_url___, what, log=self.request.sdiapi.flash, auth=self.auth)
        if self.performance is not None:
            status_code = None
            if result is not None:
                status_code = result.status_code
            try:
                self.performance.stop(self.request.host, self.request.path, self.request.url, self.__solr_url___, params, what, status_code)  # nopep8
            except:
                self.request.sdiapi.flash('Log error: %s\n%s' % (what, sys.exc_info()), 'error')
            else:
                url = "http://localhost:6543/spoke/performance?tab=sheet&q=*&fq=UUID:%22"  # nopep8
                url += self.performance.payload[0]['UUID']
                url += "%22&fq=PATH_URL:%22"
                url += quote(self.request.path.encode('utf8'))
                url += "%22"
                seconds = self.performance.payload[0]['SECONDS']
                if status_code == 200:
                    level = 'info'
                else:
                    level = 'error'
                self.request.sdiapi.flash('Log: <a href="%s">%s</a>: %s' % (url, what, seconds), level)  # nopep8
        return result

    def get_all_facet_counts(self, facet_field):
        # Use the precalculated geo part dictionary as a start
        params = self.geo_part_dict.copy()
        for fq in self.query.params.getall('fq'):
            if ':' not in fq:
                continue
            key, value = fq.split(':', 1)
            if key in self.model['geo_fields']:
                continue
            if 'd' in self.query.params and key == self.model['id_key']:
                params.add('fq', key + ':[*%20TO%20*]%20-id:(' + '%s' % value + ')')
            if value.startswith('"') and value.endswith('"'):
                value = quote(value[1:-1].encode('utf8'))
                params.add('fq', '%s:"%s"' % (key, value))
        # Add the search value entered by the user
        params['q'] = self.search_value
        # Do not include the result rows
        params['rows'] = '0'
        # Do a facet search
        params['facet'] = 'true'
        # Add all facet.field parameter
        params.add('facet.field', facet_field)
        # Set facet.limit parameter to no limit
        params.add('facet.limit', '-1')
        # Exclude 0 in facet search
        params.add('facet.mincount', '1')
        # Expect the result in JSON format
        params['wt'] = 'json'
        return self.get('select', params, "Get counts for Facets")

    def get_facets(self):
        # Use the precalculated geo part dictionary as a start
        params = self.geo_part_dict.copy()
        # Do a facet search
        params['facet'] = 'true'
        # Expect the result in JSON format
        params['wt'] = 'json'
        # Add the search value entered by the user
        params['q'] = self.search_value
        # Do not include the result rows
        params['rows'] = '0'
        params['facet.missing'] = 'true'
        # Add all facet.field parameters
        for facet_field in self.model['facet_fields']:
            params.add('facet.field', facet_field)
        # Add all facet.field parameters
        for facet_field in self.model['technical_facet_fields']:
            if not ('facet.field', facet_field) in params.items():
                params.add('facet.field', facet_field)
        # Add start parameter depending on batch size
        if 'start' in self.query.params:
            batch_size = self.result['batch_size']
            start = self.query.params['start']
            params['start'] = str(batch_size * (int(start) - 1))
        for fq in self.query.params.getall('fq'):
            if ':' not in fq:
                continue
            if fq.split(':')[0] in self.model['geo_fields']:
                # Has been prepared in geo_part_dict
                continue
            key, value = fq.split(':', 1)
            if 'd' in self.query.params and fq.split(':')[0] == self.model['id_key']:  # nopep8
                params.add('fq', key + ':[*%20TO%20*]%20-id:(' + '%s' % value + ')')
                continue
            if value.startswith('"') and value.endswith('"'):
                if len(value) == 2:
                    params.add('fq', '-%s:["" TO *]' % key)
                else:
                    value = quote(value[1:-1].encode('utf8'))
                    params.add('fq', '%s:"%s"' % (key, value))
        return self.get('select', params, "Get Facets")

    def facet_results(self, request):
        data = get_json(request, 'Facets', self.request.sdiapi.flash)
        if data is None:
            self.result.update({'facet_fields': [],
                                'facets': {},
                                'json': {},
                                'result': ''})
            return
        facets = {}
        for facet_field in self.model['facet_fields'] + self.model['technical_facet_fields']:  # nopep8
            facets[facet_field] = []
            if 'facet_counts' not in data:
                msg = 'Facet query delivered no results: \n%s\n%s'
                info = (data.get('error'), self.__solr_url___)
                self.request.sdiapi.flash(msg % info, 'error')
                self.result.update({'facet_fields': [],
                                    'facets': {},
                                    'json': {},
                                    'result': ''})
                return
            results = data['facet_counts']['facet_fields'][facet_field]
            for index in range(0, len(results), 2):
                if results[index + 1] == 0:
                    break
                if results[index] is not None:
                    facet = {'key': results[index],
                             'count': results[index + 1]}
                    facets[facet_field].append(facet)
        facet_fields = self.model['facet_fields'][:]
        for facet_field in facet_fields[:]:
            if facets[facet_field] == []:
                del facets[facet_field]
                facet_fields.remove(facet_field)
        self.result.update({'facet_fields': facet_fields,
                            'facets': facets,
                            'json': data,
                            'result': pformat(data)})

    def get_doc(self):
        params = MultiDict()
        # Search for any
        params['q'] = '*'
        # Expect the result in JSON format
        params['wt'] = 'json'
        for fq in self.query.params.getall('fq'):
            if fq.split(':')[0] == self.model['id_key']:
                key, value = fq.split(':', 1)
                if value.startswith('"') and value.endswith('"'):
                    value = quote(value[1:-1].encode('utf8'))
                    params.add('fq', '%s:"%s"' % (key, value))
                    return self.get('select', params, "Get Document")

    def doc_results(self, request):
        data = get_json(request, 'Document', self.request.sdiapi.flash)
        if data is None:
            data = {}
        if 'response' in data and data['response']['numFound'] == 1:
            self.result['doc'] = data
        else:
            self.result['doc'] = None

    def get_docs_params(self):
        # Use the precalculated geo part dictionary as a start
        params = self.geo_part_dict.copy()
        # Expect the result in JSON format
        params['wt'] = 'json'
        # Add the search value entered by the user
        params['q'] = self.search_value
        if self.query.params.get('tab', None) == 'map' and 'd' in self.query.params:
            # This is a nearby search
            params['rows'] = '10000000'
        else:
            # Add the number of rows to be returned
            params['rows'] = str(self.result['batch_size'])
        # Add sort parameter if it has been defined in the request
        if 'sort' in self.query.params:
            params['sort'] = self.query.params['sort']
        else:
            if len(self.model['sort_fields']) > 0:
                # Default search for first item in sort list
                params['sort'] = quote("%s %s" % (self.model['sort_fields'][0], 'asc'))
        if 'd' in self.query.params:
            params.add('q', "{!func}geodist()")
            params.add('sfield', "geo_locality")
            params.add('sort', "score%20asc")
            params.add('fq', "{!geofilt}")
            params['fl'] = "*,score"
        for fq in self.query.params.getall('fq'):
            if ':' not in fq:
                continue
            if fq.split(':')[0] in self.model['geo_fields']:
                continue
            key, value = fq.split(':', 1)
            if 'd' in self.query.params and fq.split(':')[0] == self.model['id_key']:  # nopep8
                params.add('fq', key + ':[*%20TO%20*]%20-id:(' + '%s' % value + ')')
                continue
            if value.startswith('"') and value.endswith('"'):
                if len(value) == 2:
                    params.add('fq', '-%s:["" TO *]' % key)
                else:
                    value = quote(value[1:-1].encode('utf8'))
                    params.add('fq', '%s:"%s"' % (key, value))

        # Add start parameter depending on batch size
        if 'start' in self.query.params:
            params['start'] = str(self.result['batch_size'] * (int(self.query.params['start']) - 1))  # nopep8
        return params

    def get_docs(self):
        params = self.get_docs_params()
        return self.get('select', params, "Get Documents")

    def docs_results(self, request):
        data = get_json(request, 'Documents', self.request.sdiapi.flash)
        if data is None:
            data = {}
        if 'response' in data:
            num = data['response']['numFound']
            self.result['table'] = data
            # XXX
            if self.query.params.get('tab', None) == 'map' and ('d' in self.query.params or 'scientificName' in self.query.fqs):
                pass
            else:
                self.result['batch'] = self.get_batch(num, self.result['batch_size'])  # nopep8
        else:
            self.result['batch'] = None
            self.result['table'] = {'response': {'numFound': 0, 'docs': []}}

    def get_driller(self):
        # Use the precalculated geo part dictionary as a start
        params = self.geo_part_dict.copy()
        # Expect the result in CSV format
        params['wt'] = 'csv'
        # CSV tab separated
        params['csv.separator'] = '%09'
        # CSV tab separated
        params['charset'] = 'utf-8'
        # CSV tab separated
        params['rows'] = '10000000'
        # Add the search value entered by the user
        params['q'] = self.search_value
        # Override sort parameter when geo searching
        if 'd' in self.query.params:
            params.add('q', "{!func}geodist()")
            params.add('sfield', "geo_locality")
            params.add('sort', "score%20asc")
            params.add('fq', "{!geofilt}")
            fls = ['score']
        else:
            fls = []
        for fq in self.query.params.getall('fq'):
            if ':' in fq:
                key, value = fq.split(':', 1)
                if key in self.model['geo_fields']:
                    continue
                if 'd' in self.query.params and key == self.model['id_key']:
                    params.add('fq', key + ':[*%20TO%20*]%20-id:(' + '%s' % value + ')')  # nopep8
                    continue
                if value.startswith('"') and value.endswith('"'):
                    if len(value) == 2:
                        params.add('fq', '-%s:["" TO *]' % key)
                    else:
                        value = quote(value[1:-1].encode('utf8'))
                        params.add('fq', '%s:"%s"' % (key, value))
            else:
                if fq.startswith('"') and fq.endswith('"'):
                    fls.append(fq[1:-1])
        if not fls:
            for key, value in self.result['facets'].items():
                if len(value) == 0:
                    pass
                elif len(value) == 1:
                    pass
                else:
                    if key != self.model['id_key'] and key in self.result['facet_fields']:  # nopep8
                        fls.append(key)

        def filter_params(fls):
            sorted_dims = []
            for dim in fls:
                if dim in self.result['facets']:
                    sorted_dims.append((len(self.result['facets'][dim]), dim))  # nopep8
                else:
                    sorted_dims.append((0, dim))
            sorted_dims.sort()
            dims = [dim[1] for dim in sorted_dims]
            dims.reverse()
            return dims[-2:]

        fls = filter_params(fls)
        if fls:
            fls.append(self.model['id_key'])
            params.add('fl', ','.join(fls))
        else:
            return
        return self.get('select', params, "Get Driller")

    def driller_results(self, driller):
        if driller.status_code != 200:
            return
        lines = driller.text.splitlines()
        dbconn = sqlite3.connect(':memory:')
        cursor = dbconn.cursor()
        table_name = 'specimen'
        reader = csv.reader(lines, delimiter='\t')
        sql = None
        for row in reader:
            if reader.line_num == 1:
                cursor.execute('''create table %s ("%s")''' % (table_name, '","'.join(row)))  # nopep8
                names = ','.join(list('?' * len(row)))
                sql = """insert into %s values (%s)""" % (table_name, names)
                headers = row
                continue
            cursor.execute(sql, row)
        dims = [i for i in headers if i not in [self.model['id_key'], 'locality']]  # nopep8
        if len(dims) == 0:
            return
        else:
            if len(dims) == 1:
                rows = '\n'.join(dims)
                cols = ''
            else:
                sorted_dims = []
                for dim in dims:
                    if dim in self.result['facets']:
                        sorted_dims.append((len(self.result['facets'][dim]), dim))  # nopep8
                    else:
                        sorted_dims.append((0, dim))
                sorted_dims.sort()
                dims = [dim[1] for dim in sorted_dims]
                dims.reverse()
                dims = dims[-2:]
                rows = '\n'.join(dims[:-1])
                cols = dims[-1]
        id_key = self.model['id_key']
        labels = self.model['labels']
        self.result['driller'] = render_driller(rows, cols, id_key, dbconn, self.request, self.query, labels)

    def get_simple_facet_count(self):
        # Use the precalculated geo part dictionary as a start
        params = self.geo_part_dict.copy()
        # Expect the result in CSV format
        params['wt'] = 'json'
        # CSV tab separated
        params['charset'] = 'utf-8'
        # No result docs
        params['rows'] = '0'
        # Add the search value entered by the user
        params['q'] = self.search_value
        # Facet
        params['facet'] = 'true'
        # No facet limit
        params['facet.limit'] = "-1"
        # Minimum facet count is 1
        params['facet.mincount'] = '1'
        # Add sort parameter if it has been defined in the request
        if 'sort' in self.query.params:
            params['facet.sort'] = self.query.params['sort'].split()[0]
        else:
            # Sort on count
            params['facet.sort'] = 'count'
        for fq in self.query.params.getall('fq'):
            if ':' not in fq:
                if fq.startswith('"') and fq.endswith('"'):
                    params['facet.field'] = fq[1:-1]
                else:
                    params['facet.field'] = fq
                continue
            key, value = fq.split(':', 1)
            if key in self.model['geo_fields']:
                continue
            if 'd' in self.query.params and key == self.model['id_key']:
                continue
            if value.startswith('"') and value.endswith('"'):
                value = quote(value[1:-1].encode('utf8'))
                params.add('fq', '%s:"%s"' % (key, value))

        return self.get('select', params, "Get Simple Facet Count")

    def simple_facet_count_results(self, request):
        data = get_json(request, 'Simple Facet Count', self.request.sdiapi.flash)
        if data is None:
            data = {}
        try:
            facet_field = data['facet_counts']['facet_fields'].keys()[0]
        except:
            return
        batch_size = self.result['batch_size']
        start_param = int(self.query.params.get('start', '1'))
        start = batch_size * (start_param - 1) * 4
        end = start + 200 * 4
        try:
            results = data['facet_counts']['facet_fields'][facet_field][start:end]
        except:
            return
        facets = []
        for index in range(0, len(results), 2):
            if results[index + 1] == 0:
                break
            facets.append({'key': results[index], 'count': results[index + 1]})
        num = len(data['facet_counts']['facet_fields'][facet_field])
        self.result['batch'] = self.get_batch(num, batch_size * 4)
        self.result['driller'] = {'facet_field': facet_field,
                                  'simple_facet_count': facets}

    def get_fq_count(self, fq):
        # Use the precalculated geo part dictionary as a start
        params = self.geo_part_dict.copy()
        # Expect the result in CSV format
        params['wt'] = 'json'
        # CSV tab separated
        params['charset'] = 'utf-8'
        # No result docs
        params['rows'] = '0'
        # Add the search value entered by the user
        params['q'] = self.search_value
        # Facet
        params['facet'] = 'true'
        # No facet limit
        params['facet.limit'] = "-1"
        # Minimum facet count is 1
        params['facet.mincount'] = '1'
        # Sort on count
        params['facet.sort'] = 'count'
        params['facet.field'] = fq
        for fq in self.query.params.getall('fq'):
            if ':' not in fq:
                continue
            key, value = fq.split(':', 1)
            if key in self.model['geo_fields']:
                continue
            if 'd' in self.query.params and key == self.model['id_key']:
                continue
            if value.startswith('"') and value.endswith('"'):
                value = quote(value[1:-1].encode('utf8'))
                params.add('fq', '%s:"%s"' % (key, value))
        return self.get('select', params, "Get FQ Count")

    def fq_count_results(self, request, fq):
        data = get_json(request, 'FQ Count', self.request.sdiapi.flash)
        if data is None:
            data = {}
        try:
            results = data['facet_counts']['facet_fields'][fq]
        except:
            return []
        facets = []
        for index in range(0, len(results), 2):
            if results[index + 1] == 0:
                break
            facets.append({'key': results[index], 'count': results[index + 1]})
        return facets
