import unittest
from like.solr.callbacks import base_url
from webob.multidict import MultiDict


class Context:
    def __init__(self):
        self.model = {'geo_fields': ["geo_locality"],
                      'filter_fields': ["scientificName",
                                        "genus",
                                        "country",
                                        "typestatus",
                                        "collectorname",
                                        "institutioncode",
                                        "associationtype",
                                        "identifiername",
                                        "collectioncode",
                                        "format",
                                        "geo_locality"]
                      }


class Request:

    def __init__(self, params):
        self.params = params
        self.context = Context()


class BaseURLTests(unittest.TestCase):

    def test_empty(self):
        doc = {'genus': 'Trifolium'}
        params = MultiDict()
        request = Request(params)
        facet_field_key = "genus"
        filter_fields = ["genus"]
        url = base_url(request, facet_field_key, doc, filter_fields)
        expected = '?fq=genus:%22Trifolium%22'
        self.failUnless(url == expected, url)

    def test_one_asc(self):
        doc = {'genus': 'Trifolium'}
        params = MultiDict()
        params['fq'] = 'scientificName:"Cardamine hirsuta"'
        request = Request(params)
        facet_field_key = "genus"
        filter_fields = ["genus"]
        url = base_url(request, facet_field_key, doc, filter_fields)
        expected = '?fq=scientificName%3A%22Cardamine%20hirsuta%22&fq=genus:%22Trifolium%22'  # nopep8
        self.failUnless(url == expected, url)

    def test_one_desc(self):
        doc = {'genus': 'Trifolium'}
        params = MultiDict()
        params['fq'] = 'scientificName:"Cardamine hirsuta"'
        request = Request(params)
        facet_field_key = "genus"
        filter_fields = ["genus"]
        url = base_url(request, facet_field_key, doc, filter_fields)
        expected = '?fq=scientificName%3A%22Cardamine%20hirsuta%22&fq=genus:%22Trifolium%22'  # nopep8
        self.failUnless(url == expected, url)

    def test_multi_asc(self):
        doc = {'collectioncode': 'Herbarium Willing'}
        params = MultiDict()
        params['fq'] = u'country:"Greece"'
        params['fq'] = u'genus:"Orobanche"'
        params['fq'] = u'institutioncode:"BGBM"'
        request = Request(params)
        facet_field_key = "collectioncode"
        filter_fields = ["genus", "collectioncode"]
        url = base_url(request, facet_field_key, doc, filter_fields)
        expected = '?fq=institutioncode%3A%22BGBM%22&fq=collectioncode:%22Herbarium%20Willing%22'  # nopep8
        self.failUnless(url == expected, url)

    def test_multi_desc(self):
        doc = {'collectioncode': 'Herbarium Willing'}
        params = MultiDict()
        params['fq'] = u'country:"Greece"'
        params['fq'] = u'genus:"Orobanche"'
        params['fq'] = u'institutioncode:"BGBM"'
        request = Request(params)
        facet_field_key = "collectioncode"
        filter_fields = ["genus", "collectioncode"]
        url = base_url(request, facet_field_key, doc, filter_fields)
        expected = '?fq=institutioncode%3A%22BGBM%22&fq=collectioncode:%22Herbarium%20Willing%22'  # nopep8
        self.failUnless(url == expected, url)

    def test_one_asc_sorted_same(self):
        doc = {'genus': 'Trifolium'}
        params = MultiDict()
        params['fq'] = 'scientificName:"Cardamine hirsuta"'
        params['sort'] = 'genus desc'
        request = Request(params)
        facet_field_key = "genus"
        expected = '?fq=scientificName%3A%22Cardamine%20hirsuta%22&sort=genus%20desc&fq=genus:%22Trifolium%22'  # nopep8
        filter_fields = ["genus"]
        url = base_url(request, facet_field_key, doc, filter_fields)
        self.failUnless(url == expected, url)

    def test_one_desc_sorted_same(self):
        doc = {'genus': 'Trifolium'}
        params = MultiDict()
        params['fq'] = 'scientificName:"Cardamine hirsuta"'
        params['sort'] = 'genus desc'
        request = Request(params)
        facet_field_key = "genus"
        filter_fields = ["genus"]
        url = base_url(request, facet_field_key, doc, filter_fields)
        expected = '?fq=scientificName%3A%22Cardamine%20hirsuta%22&sort=genus%20desc&fq=genus:%22Trifolium%22'  # nopep8
        self.failUnless(url == expected, url)

    def test_root(self):
        doc = {u'_version_': 1448334580902789126,
               u'highertaxon': u'LEGUMINOSAE',
               u'unitid': u'B 10 0438393',
               u'genus': u'Trifolium',
               u'id': u'273906'}
        request = Request({})
        facet_field_key = "genus"
        filter_fields = ["genus"]
        url = base_url(request, facet_field_key, doc, filter_fields)
        expected = '?fq=genus:%22Trifolium%22'
        self.failUnless(url == expected, url)

    def test_geo_locality(self):
        doc = {'geo_locality': '8.13722222222,48.6966666667'}
        params = MultiDict()
        params['fq'] = 'scientificName:"Cardamine hirsuta"'
        params['sort'] = 'genus desc'
        request = Request(params)
        facet_field_key = "geo_locality"
        filter_fields = ["genus", "collectioncode", "scientificName", "geo_locality"]  # nopep8
        url = base_url(request, facet_field_key, doc, filter_fields)
        expected = '?fq=scientificName%3A%22Cardamine%20hirsuta%22&sort=genus%20desc&fq=geo_locality:%228.13722222222%2C48.6966666667%22'  # nopep8
        self.failUnless(url == expected, url)
