import unittest
from like.solr.callbacks import sort_url
from webob.multidict import MultiDict


class Context:
    def __init__(self):
        self.id_key = 'id'
        self.model = {'sort_fields': ["rawidentificationid",
                                      "collectorname",
                                      "scientificName",
                                      "genus",
                                      "typestatus",
                                      "institutioncode",
                                      "format",
                                      "collectioncode"]}


class Request:

    def __init__(self, params):
        self.params = params
        self.context = Context()


class SortURLTests(unittest.TestCase):

    def test_empty(self):
        params = MultiDict()
        request = Request(params)
        facet_field_key = "genus"
        direction = "asc"
        sort_fields = ["collectioncode", "genus", "scientificName"]
        url = sort_url(request, facet_field_key, direction, sort_fields)
        expected = "?sort=genus%20asc"
        self.failUnless(url == expected, url)

    def test_one_asc(self):
        params = MultiDict()
        params['fq'] = ['scientificName:"Cardamine hirsuta"']
        request = Request(params)
        facet_field_key = "genus"
        direction = "asc"
        sort_fields = ["collectioncode", "genus", "scientificName"]
        url = sort_url(request, facet_field_key, direction, sort_fields)
        expected = """?sort=genus%20asc&fq=['scientificName:"Cardamine hirsuta"']"""
        self.failUnless(url == expected, url)

    def test_one_desc(self):
        params = MultiDict()
        params['fq'] = 'scientificName:"Cardamine%20hirsuta"'
        request = Request(params)
        facet_field_key = "genus"
        direction = "asc"
        sort_fields = ["collectioncode", "genus", "scientificName"]
        url = sort_url(request, facet_field_key, direction, sort_fields)
        expected = '?sort=genus%20asc&fq=scientificName%3A%22Cardamine%2520hirsuta%22'
        self.failUnless(url == expected, url)

    def test_multi_asc(self):
        params = MultiDict()
        params['fq'] = u'country:"Greece"'
        params['fq'] = u'genus:"Orobanche"'
        params['fq'] = u'institutioncode:"BGBM"'
        request = Request(params)
        facet_field_key = "collectioncode"
        direction = "asc"
        sort_fields = ["collectioncode", "genus", "scientificName"]
        url = sort_url(request, facet_field_key, direction, sort_fields)
        expected = "?sort=collectioncode%20asc&fq=institutioncode%3A%22BGBM%22"
        self.failUnless(url == expected, url)

    def test_multi_desc(self):
        params = MultiDict()
        params['fq'] = u'country:"Greece"'
        params['fq'] = u'genus:"Orobanche"'
        params['fq'] = u'institutioncode:"BGBM"'
        request = Request(params)
        facet_field_key = "collectioncode"
        direction = "asc"
        sort_fields = ["collectioncode", "genus", "scientificName"]
        url = sort_url(request, facet_field_key, direction, sort_fields)
        expected = '?sort=collectioncode%20asc&fq=institutioncode%3A%22BGBM%22'
        self.failUnless(url == expected, url)

    def test_one_asc_sorted_same(self):
        params = MultiDict()
        params['fq'] = 'scientificName:"Cardamine hirsuta"'
        params['sort'] = 'genus desc'
        request = Request(params)
        facet_field_key = "genus"
        direction = "asc"
        sort_fields = ["collectioncode", "genus", "scientificName"]
        url = sort_url(request, facet_field_key, direction, sort_fields)
        expected = '?sort=genus%20asc&fq=scientificName%3A%22Cardamine%20hirsuta%22'
        self.failUnless(url == expected, url)

    def test_one_desc_sorted_same(self):
        params = MultiDict()
        params['fq'] = 'scientificName:"Cardamine hirsuta"'
        params['sort'] = 'genus desc'
        request = Request(params)
        facet_field_key = "genus"
        direction = "desc"
        sort_fields = ["collectioncode"]
        url = sort_url(request, facet_field_key, direction, sort_fields)
        self.failUnless(url is None, url)

    def test_sort_1(self):
        params = MultiDict()
        request = Request(params)
        facet_field_key = "collectioncode"
        direction = "desc"
        sort_fields = ['collectioncode']
        url = sort_url(request, facet_field_key, direction, sort_fields)
        expected = None
        self.failUnless(url == expected, url)

    def test_sort_2(self):
        """
        Return None when facet field is not in sort fields
        """
        params = MultiDict()
        request = Request(params)
        facet_field_key = "collectioncode"
        direction = "desc"
        sort_fields = []
        url = sort_url(request, facet_field_key, direction, sort_fields)
        expected = None
        self.failUnless(url == expected, url)

    def test_sort_3(self):
        """
        Filter out some request variables like sort, start and page
        """
        params = MultiDict()
        params['sort'] = 'What'
        params['start'] = 'What'
        params['page'] = 'What'
        request = Request(params)
        facet_field_key = "collectioncode"
        direction = "desc"
        sort_fields = ["collectioncode"]
        url = sort_url(request, facet_field_key, direction, sort_fields)
        expected = None
        self.failUnless(url == expected, url)

    def test_sort_4(self):
        """
        Ascending
        """
        params = MultiDict()
        request = Request(params)
        facet_field_key = "collectioncode"
        direction = "asc"
        sort_fields = ["collectioncode"]
        url = sort_url(request, facet_field_key, direction, sort_fields)
        expected = "?sort=collectioncode%20asc"
        self.failUnless(url == expected, url)

    def test_sort_5(self):
        """
        Already sorting in asc direction, so expect None
        """
        params = MultiDict()
        params['sort'] = "collectioncode asc"
        request = Request(params)
        facet_field_key = "collectioncode"
        direction = "asc"
        sort_fields = ["collectioncode"]
        url = sort_url(request, facet_field_key, direction, sort_fields)
        expected = None
        self.failUnless(url == expected, url)

    def test_sort_6(self):
        """
        Already sorting in desc direction, no link
        """
        params = MultiDict()
        params['sort'] = "collectioncode desc"
        request = Request(params)
        facet_field_key = "collectioncode"
        direction = "desc"
        sort_fields = ["collectioncode"]
        url = sort_url(request, facet_field_key, direction, sort_fields)
        expected = None
        self.failUnless(url == expected, url)

    def test_sort_7(self):
        """
        Change sort direction from desc to asc
        """
        params = MultiDict()
        params['sort'] = "collectioncode desc"
        request = Request(params)
        facet_field_key = "collectioncode"
        direction = "asc"
        sort_fields = ["collectioncode"]
        url = sort_url(request, facet_field_key, direction, sort_fields)
        expected = "?sort=collectioncode%20asc"
        self.failUnless(url == expected, url)

    def test_sort_8(self):
        """
        Change sort direction from asc to desc
        """
        params = MultiDict()
        params['sort'] = "collectioncode asc"
        request = Request(params)
        facet_field_key = "collectioncode"
        direction = "desc"
        sort_fields = ["collectioncode"]
        url = sort_url(request, facet_field_key, direction, sort_fields)
        expected = "?sort=collectioncode%20desc"
        self.failUnless(url == expected, url)
