try:
    import unittest2 as unittest
except ImportError:
    import unittest  # NOQA
from webob.multidict import MultiDict
from like.solr.solr import join_params
from like.solr.solr import Solr


class TestJoinParams(unittest.TestCase):

    def test_join_params_1(self):
        params = {}
        result = join_params(params)
        self.failUnless(result == '', result)

    def test_join_params_2(self):
        params = {}
        inject = 'inject=True'
        result = join_params(params, inject=inject)
        self.failUnless(result == '?inject=True', result)

    def test_join_params_3(self):
        params = {'q': 'searchquery'}
        inject = 'inject=True'
        result = join_params(params, inject=inject)
        self.failUnless(result == '?inject=True&q=searchquery', result)

    def test_join_params_4(self):
        params = {'q': ['searchquery', 'another']}
        inject = 'inject=True'
        result = join_params(params, inject=inject)
        self.failUnless(result == '?inject=True&q=searchquery&q=another', result)

    def test_join_params_5(self):
        params = {'q': ['searchquery', 'another'], 't': "someother"}
        inject = 'inject=True'
        result = join_params(params, inject=inject)
        self.failUnless(result == '?inject=True&q=searchquery&q=another&t=someother', result)


class Registry(object):

    def __init__(self):
        self.settings = {'like.solr.server': None,
                         'like.solr.model': None,
                         'performance.log': None
                         }


class SDIAPI(object):
    def flash(self, msg, level):
        self.msg = msg
        self.level = level


class Request(object):
    def __init__(self):
        self.params = MultiDict()
        self.registry = Registry()
        self.sdiapi = SDIAPI()


class TestSolr(unittest.TestCase):

    def test_init_1(self):
        request = Request()
        solr = Solr(request)
        self.failUnless(solr.SERVER is None)
        self.failUnless(solr.auth is None)
        self.failUnless(solr.performance is None)

    def test_init_performance_2(self):
        request = Request()
        solr = Solr(request, performance_logging=True)
        self.failUnless(solr.SERVER is None)
        self.failUnless(solr.auth is None)
        self.failUnless(solr.performance.target == 'update?commit=true')


class TestSolrGetSolrURL(unittest.TestCase):

    def test_getsolrurl_1(self):
        request = Request()
        request.registry.settings['like.solr.model'] = {'inject': ''}
        solr = Solr(request)
        result = solr.get_solr_url('', {})
        self.failUnless(result == '', result)

    def test_getsolrurl_2(self):
        request = Request()
        request.registry.settings['like.solr.model'] = {'inject': ''}
        solr = Solr(request, server='http://localhost:6543')
        result = solr.get_solr_url('', {})
        self.failUnless(result == 'http://localhost:6543', result)

    def test_getsolrurl_3(self):
        request = Request()
        request.registry.settings['like.solr.model'] = {'inject': ''}
        solr = Solr(request, server='http://localhost:6543')
        result = solr.get_solr_url('Get', {})
        self.failUnless(result == 'http://localhost:6543/Get', result)

    def test_getsolrurl_4(self):
        request = Request()
        request.registry.settings['like.solr.model'] = {'inject': 'k=2'}
        solr = Solr(request, server='http://localhost:6543')
        result = solr.get_solr_url('Get', {})
        self.failUnless(result == 'http://localhost:6543/Get?k=2', result)


class TestSolrGet(unittest.TestCase):

    def test_get_1(self):
        request = Request()
        request.registry.settings['like.solr.model'] = {'inject': ''}
        solr = Solr(request, server='http://localhost:6543')
        result = solr.get('get', request.params, 'This')
        self.failUnless(result is None, result)
        self.failUnless(request.sdiapi.msg == 'Connection error during: This', request.sdiapi.msg)
        self.failUnless(request.sdiapi.level == 'error', request.sdiapi.level)


class TestSolrGetAllFacetCounts(unittest.TestCase):

    def test_getallfacetcounts_1(self):
        request = Request()
        request.registry.settings['like.solr.model'] = {'inject': ''}
        solr = Solr(request, server='http://localhost:6543')
        result = solr.get_all_facet_counts('id')
        self.failUnless(result is None, result)
        self.failUnless(request.sdiapi.msg == 'Connection error during: Get counts for Facets', request.sdiapi.msg)
        self.failUnless(request.sdiapi.level == 'error', request.sdiapi.level)


class TestSolrGetFacets(unittest.TestCase):

    def test_getfacets_1(self):
        request = Request()
        request.registry.settings['like.solr.model'] = {'inject': '',
                                                        'facet_fields': [],
                                                        'technical_facet_fields': []
                                                        }
        solr = Solr(request, server='http://localhost:6543')
        result = solr.get_facets()
        self.failUnless(result is None, result)
        self.failUnless(request.sdiapi.msg == 'Connection error during: Get Facets', request.sdiapi.msg)
        self.failUnless(request.sdiapi.level == 'error', request.sdiapi.level)


class TestSolrGetFacetResults(unittest.TestCase):

    def test_facetresults_1(self):
        request = Request()
        request.registry.settings['like.solr.model'] = {'inject': '',
                                                        'facet_fields': [],
                                                        'technical_facet_fields': []
                                                        }
        solr = Solr(request, server='http://localhost:6543')
        result = solr.facet_results(None)
        self.failUnless(solr.result == {'facet_fields': [],
                                        'facets': {},
                                        'json': {},
                                        'result': ''}, result)


class TestSolrGetDoc(unittest.TestCase):

    def test_getdoc_1(self):
        request = Request()
        solr = Solr(request, server='http://localhost:6543')
        result = solr.get_doc()
        self.failUnless(result is None, result)


class TestSolrDocResults(unittest.TestCase):

    def test_docresults_1(self):
        request = Request()
        solr = Solr(request, server='http://localhost:6543')
        result = solr.doc_results(None)
        self.failUnless(result is None, result)


class TestSolrGetDocsParams(unittest.TestCase):

    def test_getdocsparams_1(self):
        request = Request()
        request.registry.settings['like.solr.model'] = {'inject': '',
                                                        'facet_fields': [],
                                                        'technical_facet_fields': []
                                                        }
        solr = Solr(request, server='http://localhost:6543')
        result = solr.facet_results(None)
        self.failUnless(solr.result == {'facet_fields': [],
                                        'facets': {},
                                        'json': {},
                                        'result': ''}, result)
 