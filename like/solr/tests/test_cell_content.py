import unittest
from like.solr.callbacks import cell_content
from webob.multidict import MultiDict


class Context:
    def __init__(self):
        self.id_key = 'id'
        self.model = {'geo_fields': ["geo_locality"],
                      'filter_fields': ["scientificName",
                                        "genus",
                                        "country",
                                        "typestatus",
                                        "collectorname",
                                        "institutioncode",
                                        "associationtype",
                                        "identifiername",
                                        "collectioncode",
                                        "format",
                                        "geo_locality"]}


class Request:

    def __init__(self, params, path_qs):
        self.params = params
        self.path_qs = path_qs
        self.context = Context()


class BaseURLTests(unittest.TestCase):

    def test_geo_locality(self):
        params = MultiDict()
        path_qs = "/"
        request = Request(params, path_qs)
        doc = {'geo_locality': '8.13722222222,48.6966666667'}
        facet_field_key = "geo_locality"
        id_key = 'id'
        query = request
        filter_fields = []
        url = cell_content(request, query, facet_field_key, doc, id_key, filter_fields)
        expected = "8.13722222222,48.6966666667"
        self.failUnless(url == expected, url)
