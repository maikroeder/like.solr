import unittest
from simplejson import JSONDecodeError
from like.solr.utils import call_log_method
from like.solr.utils import get_url
from like.solr.utils import get_json


class Log(object):

    def __init__(self):
        self.msg = None
        self.level = None

    def log(self, msg, level):
        self.msg = msg
        self.level = level


class TestUtils(unittest.TestCase):

    def test_call_log_method_1(self):
        why = "This"
        what = "That"
        log = Log()
        log_method = log.log
        call_log_method(why, what, log_method, 'debug')
        self.failUnless(log.msg == "This during: That")
        self.failUnless(log.level == "debug")

    def test_call_log_method_2(self):
        why = "This"
        what = "That"
        log = Log()
        log_method = log.log
        self.assertRaises(AttributeError, call_log_method, why, what, log_method, 'danger')


class TestGetUrl(unittest.TestCase):

    def test_get_url_1(self):
        solr_url = "http://localhost:6543/"
        what = "This"
        log = Log()
        log_method = log.log
        response = get_url(solr_url, what, log=log_method)
        self.failUnless(response is None)
        self.failUnless(log.msg == "Connection error during: This")
        self.failUnless(log.level == "error")

    def test_get_url_2(self):
        solr_url = "http://localhost:6543/"
        what = "This"
        log = Log()
        log_method = log.log
        response = get_url(solr_url, what, log=log_method)
        self.failUnless(response is None)
        self.failUnless(log.msg == "Connection error during: This")
        self.failUnless(log.level == "error")


class Response(object):
    def __init__(self):
        self.text = None

    def set_text(self, text):
        self.text = text

    def json(self):
        return self.text


class ErrorResponse(Response):
    def json(self):
        msg = ''
        doc = ''
        pos = 0
        raise JSONDecodeError(msg, doc, pos)


class TestGetJSON(unittest.TestCase):

    def test_get_url_1(self):
        response = None
        what = "This"
        log = Log()
        log_method = log.log
        data = get_json(response, what, log_method)
        self.failUnless(data is None)

    def test_get_url_2(self):
        response = Response()
        response.set_text("JSON String")
        what = "This"
        log = Log()
        log_method = log.log
        data = get_json(response, what, log_method)
        self.failUnless(data == "JSON String")

    def test_get_url_3(self):
        response = ErrorResponse()
        response.set_text("JSON String")
        what = "This"
        log = Log()
        log_method = log.log
        data = get_json(response, what, log_method)
        self.failUnless(data is None)
        self.failUnless(log.msg == "JSON error during: This", log.msg)
        self.failUnless(log.level == "error", log.level)

    def test_get_url_4(self):
        response = ErrorResponse()
        response.set_text("503 Service Unavailable")
        what = "This"
        log = Log()
        log_method = log.log
        data = get_json(response, what, log_method)
        self.failUnless(data is None)
        self.failUnless(log.msg == "Error: 503 Service Unavailable during: This", log.msg)
        self.failUnless(log.level == "error", log.level)
