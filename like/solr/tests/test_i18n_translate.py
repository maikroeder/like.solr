# -*- coding: utf-8 -*-
try:
    import unittest2 as unittest
except ImportError:
    import unittest  # NOQA
from like.solr.i18n import translate


class Request(object):
    locale_name = 'de'


class TestI18NTranslate(unittest.TestCase):

    def test_translate_dict_existing(self):
        request = Request()
        value = {u'de': u'Übersetzung'}
        translation = translate(request, value)
        self.failUnless(translation == u'Übersetzung', translation)

    def test_translate_dict_notexisting(self):
        request = Request()
        value = {u'en': u'Translation'}
        translation = translate(request, value)
        self.failUnless(translation == {u'en': u'Translation'}, translation)

    def test_translate_string_vanilla(self):
        request = Request()
        value = u'Übersetzung'
        translation = translate(request, value)
        self.failUnless(translation == u'Übersetzung', translation)

    def test_translate_string_dict(self):
        request = Request()
        value = u"""{"de":"Übersetzung"}"""
        translation = translate(request, value)
        self.failUnless(translation == u'Übersetzung', translation)

    def test_translate_string_dictnotexisting(self):
        request = Request()
        value = u"""{"en":"Translation"}"""
        translation = translate(request, value)
        self.failUnless(translation == u"""{"en":"Translation"}""", translation)
