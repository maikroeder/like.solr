try:
    import unittest2 as unittest
except ImportError:
    import unittest  # NOQA
from like.solr.sql import SqliteFromJSON
from like.solr.tests.test_solr import Request


class TestSqlSqliteFromJSON(unittest.TestCase):

    def test_1(self):
        request = Request()
        request.user = {}
        headers = ['id', 'title']
        id_key = 'id'
        json = [{'id': 1, 'title': 'Hello World'}]
        SqliteFromJSON(request, json, headers, id_key)
