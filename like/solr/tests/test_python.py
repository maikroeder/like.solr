try:
    import unittest2 as unittest
except ImportError:
    import unittest  # NOQA
from like.solr.python import Python
from like.solr.tests.test_solr import Request


class TestSqlSqliteFromCsv(unittest.TestCase):

    def test_1(self):
        request = Request()
        python = Python(request)
        self.failUnless(python.request == request)
        self.failUnless(python.geo_part == '')
        self.failUnless(python.query.fqs == {})
        self.failUnless(python.query.dims == [])
        self.failUnless(python.model is None)
        self.failUnless(python.augmentations == [])
        self.failUnless(python.result == {})
        self.failUnless(python.search_value == "")
        self.failUnless(python.search == "")
        self.failUnless(python.geo_part_dict == {})

    def test_get_facets_1(self):
        request = Request()
        python = Python(request)
        python.LINES = []
        python.get_facets()

    def test_get_facets_2(self):
        request = Request()
        python = Python(request)
        python.LINES = [{'a': '1'}]
        python.get_facets()
        self.failUnless(python.filtered == [{'a': '1'}])

    def test_get_facets_3(self):
        request = Request()
        python = Python(request)
        python.query.fqs = {'a': '1'}
        python.LINES = [{'a': '1'}]
        python.get_facets()
        self.failUnless(python.filtered == [{'a': '1'}])

    def test_get_facets_4(self):
        request = Request()
        python = Python(request)
        python.query.fqs = {'a': '2'}
        python.LINES = [{'a': '1'}]
        python.get_facets()
        self.failUnless(python.filtered == [])

    def test_facet_results(self):
        request = Request()
        python = Python(request)
        python.LINES = []
        python.model = {'facet_fields': []}
        python.result = {'batch_size': 10}
        python.facet_results(request)

    def test_get_docs(self):
        request = Request()
        python = Python(request)
        python.get_docs()

    def test_docs_results(self):
        request = Request()
        python = Python(request)
        python.LINES = []
        python.model = {'facet_fields': []}
        python.result = {'batch_size': 10}
        python.get_facets()
        python.get_docs()
        python.docs_results(request)

    def test_driller(self):
        request = Request()
        python = Python(request)
        python.driller()

    def test_get_driller(self):
        request = Request()
        python = Python(request)
        python.get_driller()

    def test_driller_results(self):
        request = Request()
        python = Python(request)
        driller = python.get_driller()
        python.driller_results(driller)
