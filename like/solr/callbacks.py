# -*- coding: utf-8 -*-
import types
try:
    from urllib.parse import urljoin
except ImportError:
    from urlparse import urljoin
try:
    from urllib.parse import quote
except ImportError:
    from urllib import quote
import random
from .i18n import translate as _


def make_link(query, page):
    params = ['start=%s' % page]
    for key, value in query.params.items():
        if key in ['start', 'page']:
            continue
        try:
            value = quote(value.encode('utf8'))
        except:
            value = str(value)
        params.append(key + '=' + value)
    return '?' + '&'.join(params)


def get_hidden(query):
    hidden = []
    for key, value in query.params.items():
        if key == 'page':
            continue
        if key == 'search_term' and 'search_term' in query.params:
            # Searching twice is not allowed
            continue
        try:
            value = value.decode('utf8')
        except:
            pass
        hidden.append({'key': key, 'value': value})
    return hidden


def exclude_facet(query, facet, id_key):
    params = []
    for key, value in query.params.items():
        if key == 'page':
            continue
        if key in ['context', 'tab'] and facet == id_key:
            # Forget context and tab when id is removed
            continue
        elif ':' in value:
            k = value.split(':', 1)[0]
            if facet != k:
                try:
                    value = quote(value.encode('utf8'))
                except:
                    value = str(value)
                params.append(key + '=' + value)
        else:
            if key == 'search_term' and facet == 'search_term':
                pass
            elif key == 'geohash' and facet == 'geohash':
                pass
            else:
                try:
                    value = quote(value.encode('utf8'))
                except:
                    value = str(value)
                params.append(key + '=' + value)
    return '?' + '&'.join(params)


def selected_facets(params, id_key):
    facets = []
    for key, value in params.items():
        if key in ['page', id_key]:
            continue
        if key == 'fq' and ':' in value:
            k, v = value.split(':', 1)  # pylint: disable=C0103
            if v.endswith('"') and v.startswith('"'):
                facets.append({'key': k, 'value': v[1:-1]})
    if 'search_term' in params:
        facets.append({'key': 'search_term', 'value': params.get('search_term', '')})
    if 'geohash' in params:
        facets.append({'key': 'geohash', 'value': params.get('geohash', '')})
    return facets


def facet_url(query, facet_field_key, facet_field_value):
    params = []
    for key, value in query.params.items():
        if key == 'page':
            continue
        elif key == 'search_term':
            pass
        elif key == 'fq' and ':' in value:
            if facet_field_key == value.split(':')[0]:
                return False
        try:
            value = quote(value.encode('utf8'))
        except:
            value = str(value)
        params.append(key + '=' + value)
    try:
        quoted = quote(facet_field_value.encode('utf8'))
    except:
        quoted = str(facet_field_value)
    params.append('fq=%s:"%s"' % (facet_field_key, quoted))
    return '?' + '&'.join(params)


def base_url(query, facet_field_key, doc, filter_fields):
    """
    Clicking on a cell adds the value in the doc of the facet_field_key given.
    """
    if 'tab' in query.params:
        if query.params['tab'] == 'info':
            return None

    # Only filter fields are linked
    if facet_field_key not in filter_fields:
        return None

    params = []
    for key, value in query.params.items():
        if key in ['start', 'page']:
            # Get rid of any batching, because with the new parameter
            # this is bound to change
            continue
        elif key == 'fq' and ':' in value:
            if facet_field_key == value.split(':')[0]:
                continue
        try:
            value = quote(value.encode('utf8'))
        except:
            value = str(value)
        params.append(key + '=' + value)

    # Get the value from the doc
    facet_field_value = doc.get(facet_field_key, '')
    try:
        value = quote(facet_field_value.encode('utf8'))
    except:
        value = str(facet_field_value)
    # Link to the more refined search
    params.append('fq=' + facet_field_key + ':%22' + value + '%22')
    return '?' + '&'.join(params)


def sort_url(query, facet_field_key, direction, sort_fields):
    """
    Filter out any existing sorting parameter and insert a fresh one

    ascending: A to Z and 0 to 9
    descending Z to A and 9 to 0

    > print query.params
    > NestedMultiDict([(u'/', u''), (u'sort', u'country asc')])
    """
    if facet_field_key not in sort_fields:
        return None
    if (u'sort', u'%s %s' % (facet_field_key, direction)) in query.params.items():  # nopep8
        # the sorting is already in this direction
        return None
    elif (u'sort', u'%s %s' % (facet_field_key, {'asc': 'desc', 'desc': 'asc'}[direction])) in query.params.items():  # nopep8
        # stick to direction if inverse is current
        pass
    elif direction == 'desc':
        # Ignore, ascending is the default
        return None
    params = []
    value = quote("%s %s" % (facet_field_key, direction))
    params = ["sort=%s" % value]
    for key, value in query.params.items():
        if key in ['sort', 'start', 'page']:
            continue
        try:
            value = quote(value.encode('utf8'))
        except:
            value = str(value)
        params.append(key + '=' + value)
    if params == []:
        return ''
    else:
        return '?' + '&'.join(params)


def sort_arrow(query, facet_field_key, direction, sort_fields):
    """ Product the sort arrow for sorting fields
    """
    if facet_field_key not in sort_fields:
        return u''
    if facet_field_key == sort_fields[0]:
        if 'sort' not in query.params:
            # We found the default sorting field, which is the first in the sort fields list
            # And there is no other field to sort on
            return u'&#9660;'
    if (u'sort', u'%s %s' % (facet_field_key, direction)) in query.params.items():  # nopep8
        # the sorting is in this direction, and this has to be shown visually
        if direction == 'asc':
            html = u'&#9660;'
        else:
            html = u'&#9650;'
    elif (u'sort', u'%s %s' % (facet_field_key, {'asc': 'desc', 'desc': 'asc'}[direction])) in query.params.items():  # nopep8
        # change direction of sorting for this facet field
        if direction == 'asc':
            html = u'&#9650;'
        else:
            html = u'&#9660;'
    else:
        # The field is not sorted up or down, so make the sort choice available on hovering
        # change direction of sorting for this facet field
        html = u'<span class="sort-arrow">&#9660;</span>'
    return html


def id_url(query, id_key, doc, tab="info"):
    """
    Clicking on view icon to see a single entry by id
    """
    params = ['tab=%s' % tab, 'context=specimen']
    for key, value in query.params.items():
        if key in ['tab', 'context']:
            continue
        if key == 'fq':
            if ':' in value:
                k = value.split(':', 1)[0]
                if k == id_key:
                    # Existing id is ignored
                    continue
            try:
                value = quote(value.encode('utf8'))
            except:
                value = str(value)
            params.append(key + '=' + value)

    # Get the value from the doc
    facet_field_value = doc.get(id_key, '')
    try:
        value = quote(facet_field_value.encode('utf8'))
    except:
        value = str(facet_field_value)

    # Link to the more refined search
    params.append('fq=' + id_key + ':%22' + value + '%22')
    return '?' + '&'.join(params)


def cell_content(request, query, facet_field, doc, id_key, filter_fields, specific_part=None, image_handling='fixed'):
    """
    Links to Google maps and searches in the area
    """
    if facet_field == id_key:
        url = id_url(query, facet_field, doc)
        html = """<a href="%s"><span class="glyphicon glyphicon-%s"></span></a>""" % (url, 'eye-open')  # nopep8
        return html
    else:
        href = base_url(query, facet_field, doc, filter_fields)
        if href is None:
            return doc.get(facet_field, '')
        else:
            return """<a href="%s">%s</a>""" % (href, doc.get(facet_field, ''))


def driller_link(query, facet_field, alphabetical=False):
    params = ['tab=dashboard']
    if alphabetical is True:
        params.append('sort=%s' % quote("%s %s" % (facet_field, 'asc')))
    found = False
    for key, value in query.params.items():
        if key in ['tab', 'start', 'page']:
            continue
        if alphabetical is True and key == 'sort':
            continue
        if value.endswith('"') and value.startswith('"'):
            if key == "fq" and value[1:-1] == facet_field:
                found = True
                continue
        try:
            value = quote(value.encode('utf8'))
        except:
            value = str(value)
        params.append(key + '=' + value)
    if not found:
        params.append('fq=' + '"%s"' % facet_field)
    return '?' + '&'.join(params)


def tab_link(query, tab):
    params = ['tab=%s' % tab]
    for key, value in query.params.items():
        if key in ['tab', 'page']:
            continue
        if key == 'fq':
            try:
                value = quote(value.encode('utf8'))
            except:
                value = str(value)
        params.append(key + '=' + value)
    return '?' + '&'.join(params)


def nearby_link(query, doc, dist, id_key):
    params = ['tab=map', 'context=nearby']
    params.append('d=%s' % dist)
    for key, value in query.params.items():
        if key in ['tab', 'context', 'd']:
            continue
        if key == 'fq' and ':' in value:
            if value.split(':')[0] == id_key:
                try:
                    value = quote(value.encode('utf8'))
                except:
                    value = str(value)
                params.append(key + '=' + value)
    return '?' + '&'.join(params)


def action_link(query, action, where, num=None):
    params = []
    for key, value in query.params.items():
        try:
            value = quote(value.encode('utf8'))
        except:
            value = str(value)
        params.append(key + '=' + value)
    params = '&'.join(params)
    if params:
        params = '?' + params
    if num is None:
        url = urljoin('/action/%s/' % action, where) + params  # nopep8
    else:
        url = urljoin('/action/%s/%s/' % (action, num), where) + params  # nopep8
    return url


def geohash_url(query, id_key, geohash):
    params = ['geohash=%s' % geohash, 'tab=list']
    for key, value in query.params.items():
        if key in ['tab', 'start', 'page', 'd', 'geohash', 'context']:
            continue
        if key == 'fq':
            if ':' in value:
                k = value.split(':', 1)[0]
                if k == id_key:
                    # Existing id is ignored
                    continue
            try:
                value = quote(value.encode('utf8'))
            except:
                value = str(value)
            params.append(key + '=' + value)
    return '?' + '&'.join(params)


def advanced_search_url(query, search_term_id):
    params = []
    for key, value in query.params.items():
        if key == 'advanced_search':
            continue
        if key == 'advanced_search_term':
            continue
        try:
            value = quote(value.encode('utf8'))
        except:
            value = str(value)
        params.append(key + '=' + value)
    if not search_term_id == 'search_term':
        params.append('advanced_search=%s' % search_term_id)
    if params:
        return '?' + '&'.join(params)
    else:
        return ''


def search_info(request, query, driller, table, batch, labels):
    num = 0
    try:
        num = int(table['response']['numFound'])
    except:
        pass
    batch_length = None
    try:
        batch_length = '{0:,}'.format(batch.sequence_length)
    except:
        pass
    html = []
    if (not query.params.get('tab', None) == 'dashboard') or (query.params.get('tab', None) == 'dashboard' and not isinstance(driller, types.DictType)):
        if query.params.get('context', None) == 'specimen':
            html.append('1')
        else:
            html.append('{0:,}'.format(num))
        html.append(' ')
        if query.params.get('context', None) == 'nearby':
            html.append(' ')
            html.append(_(request, {'en': u'additional results in a radius of',
                                    'fr': u'résultats additionels dans un radius de',
                                    'de': u'zusätzliche Treffer im Radius von'}))
        else:
            if num == 1:
                html.append(_(request, {'en': u'result', 'fr': u'résultat', 'de': u'Treffer'}))
            else:
                html.append(_(request, {'en': u'results', 'fr': u'résultats', 'de': u'Treffer'}))
        if query.params.get('context', None) == 'nearby':
            html.append(' ')
            html.append(query.params.get('d', 0))
            html.append(' ')
            html.append('km')
        elif 'geohash' in query.params:
            html.append(' ')
            html.append(_(request, {'en': u'in this geographic zone',
                                    'fr': u'dans cette zone géographique',
                                    'de': u'in dieser geographischen Zone'}))
        html.append('. ')
    if batch_length is not None:
        if (query.params.get('tab', None) == 'dashboard' and 'fq' in query.params) and not (query.params.get('tab', None) == 'dashboard' and not isinstance(driller, types.DictType)):
            html.append(_(request, {'en': u'Count', 'fr': u'Compte', 'de': u'Anzahl'}))
            html.append(': ')
            html.append(batch_length)
            html.append('.')
        if (query.params.get('tab', None) != 'dashboard' or 'fq' in query.params) and not (query.params.get('tab', None) == 'dashboard' and not isinstance(driller, types.DictType)):
            if batch.multiple_pages:
                html.append("""<div class="dropdown" style="display: inline;">""")
                html.append('<button class="btn btn-default dropdown-toggle" type="button" id="dropdownbatchsize" data-toggle="dropdown">')
                html.append('{0:,}'.format(batch._size))
                html.append("""<span class="caret"></span>""")
                html.append('</button>')
                html.append("""
<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownbatchsize">
<li role="presentation"><a role="menuitem" tabindex="-1" href="/batch-size-10">10</a></li>
<li role="presentation"><a role="menuitem" tabindex="-1" href="/batch-size-25">25</a></li>
<li role="presentation"><a role="menuitem" tabindex="-1" href="/batch-size-50">50</a></li>
<li role="presentation"><a role="menuitem" tabindex="-1" href="/batch-size-100">100</a></li>
</ul>""")
                html.append('</div>')
                html.append(' ')
                html.append(_(request, {'en': u'results per page', 'fr': u'résultats par page', 'de': u'Treffer pro Seite'}))
                html.append('. ')
                html.append('{0:,}'.format(batch.numpages))
                html.append(' ')
                html.append(_(request, {'en': u'pages', 'fr': u'pages', 'de': u'Seiten'}))
                html.append('.')

    dim_choices = []
    for dim in ['search_term', 'scientificName', 'genus', 'collectioncode', 'collectorname', 'identifiername', 'country', 'locality']:
        search_link = advanced_search_url(query, dim)
        if dim == 'search_term':
            label = 'Full Text'
        else:
            label = labels.get(dim, dim)
        dim_choices.append("""<li role="presentation"><a role="menuitem" tabindex="-1" href="%s">%s</a></li>""" % (search_link, label))

    html.append("""
    <ul class="nav nav-pills" role="tablist" style="float: right;">
      <li role="presentation" class="dropdown">
        <a id="drop4" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
          Advanced Search
          <span class="caret"></span>
        </a>
        <ul id="menu1" class="dropdown-menu" role="menu" aria-labelledby="drop4">
            %s
        </ul>
      </li>
    </ul>""" % ''.join(dim_choices))

    return ''.join(html)


def autocomplete_path(request, query):
    field = query.params.get('advanced_search', 'search_term')

    params = []
    for key, value in query.params.items():
        if key == 'advanced_search':
            continue
        if key == 'advanced_search_term':
            continue
        try:
            value = quote(value.encode('utf8'))
        except:
            value = str(value)
        params.append(key + '=' + value)
    if params:
        query_string = '?' + '&'.join(params)
    else:
        query_string = ''

    if query_string:
        return 'var autocomplete_url="/autocomplete/%s' % field + request.path + query_string + '&advanced_search_term=%QUERY";'
    else:
        return 'var autocomplete_url="/autocomplete/%s' % field + request.path + '?advanced_search_term=%QUERY";'


def get_search_input(request, query, search_term):

    if 'advanced_search' in request.params and 'advanced_search_term' not in request.params:
        search_input = {'id': 'search_term',
                        'name': 'advanced_search_term',
                        'placeholder': request.params.get('advanced_search_term', ''),
                        'submit': {'en': u'Find', 'fr': u'Trouver', 'de': u'Finden'}}
    elif 'advanced_search' not in request.params:
        search_input = {'id': 'search_term',
                        'name': 'search_term',
                        'placeholder': query.params.get('search_term', ''),
                        'submit': {'en': u'Find', 'fr': u'Trouver', 'de': u'Finden'}}
    else:
        search_input = {'id': 'search_term',
                        'name': 'advanced_search_term',
                        'placeholder': search_term,
                        'submit': {'en': u'Find', 'fr': u'Trouver', 'de': u'Finden'}}
    return search_input


def edit_url(query, facet_field, alphabetical=False):
    params = ['tab=edit']
    if alphabetical is True:
        params.append('sort=%s' % quote("%s %s" % (facet_field, 'asc')))
    found = False
    for key, value in query.params.items():
        if key in ['tab', 'start', 'page']:
            continue
        if alphabetical is True and key == 'sort':
            continue
        if value.endswith('"') and value.startswith('"'):
            if key == "fq" and value[1:-1] == facet_field:
                found = True
                continue
        try:
            value = quote(value.encode('utf8'))
        except:
            value = str(value)
        params.append(key + '=' + value)
    if not found:
        params.append('fq=' + '"%s"' % facet_field)
    return '?' + '&'.join(params)
