"""
This is the like.solr Python module.
"""
from .python import Python
from .python import PythonFromFile
from .solr import Solr
from .sql import Sql
from .sql import SqliteFromJSON
from .sql import SqliteFromDB