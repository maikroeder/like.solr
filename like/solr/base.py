import math
try:
    from urllib.parse import quote
except ImportError:
    from urllib import quote
from .geohash import bbox
from webob.multidict import MultiDict
from plone.batching import Batch
from pyramid.config import Configurator
from pyramid.renderers import get_renderer
from .callbacks import facet_url
from .callbacks import base_url
from .callbacks import sort_url
from .callbacks import sort_arrow
from .callbacks import selected_facets
from .callbacks import exclude_facet
from .callbacks import get_hidden
from .callbacks import make_link
from .callbacks import driller_link
from .callbacks import tab_link
from .callbacks import nearby_link
from .callbacks import action_link
from .callbacks import id_url
from .callbacks import search_info
from .callbacks import autocomplete_path
from .callbacks import get_search_input
from .callbacks import edit_url
try:
    xrange
except:
    xrange = range

def chunks(l, n):
    """ Yield successive n-sized chunks from l.
    http://stackoverflow.com/questions/312443/how-do-you-split-a-list-into-evenly-sized-chunks-in-python
    """
    n = int(n) or 1
    for i in xrange(0, len(l), n):
        yield l[i: i + n]


class Query(object):

    def __init__(self, request_params):
        self.fqs = MultiDict()
        self.dims = []
        self.params = MultiDict()
        for key, value in request_params.items():
            if key == 'advanced_search_term' and 'advanced_search' in request_params:
                self.fqs.add(value, request_params.get('advanced_search'))
                self.params.add('fq', '%s:"%s"' % (request_params.get('advanced_search'), value))
            elif key == 'advanced_search' and 'advanced_search_term' in request_params:
                pass
            elif key == 'fq':
                if ':' not in value:
                    if value.startswith('"') and value.endswith('"'):
                        self.dims.append(value[1:-1])
                    else:
                        self.dims.append(value)
                else:
                    fq_key, fq_value = value.split(':', 1)
                    if fq_value.startswith('"') and fq_value.endswith('"'):
                        fq_value = fq_value[1:-1]
                    self.fqs.add(fq_key, fq_value)
                self.params.add(key, value)
            else:
                self.params.add(key, value)


class Base(object):  # pylint: disable=R0902

    model = None

    def __init__(self, request):
        self.__name__ = ''
        self.__parent__ = None
        self.request = request
        self.geo_part = ""
        self.query = Query(request.params)
        cell_content_dotted = request.registry.settings['like.solr.cell_content']
        config = Configurator()
        self.cell_content = config.maybe_dotted(cell_content_dotted)
        self.augmentations = []
        self.result = {}
        self.search_value = ""
        self.search = ""
        self.geo_part_dict = MultiDict()

    def get_result(self):
        path = self.model.get('main_template', 'like.solr:templates/main_template.pt')  # nopep8
        main_template = get_renderer(path).implementation()
        batch_size = self.request.cookies.get('batch_size', self.model.get('batch_size'))
        try:
            batch_size = int(batch_size)
        except:
            batch_size = self.model.get('batch_size')
        return {'facet_fields': self.model['facet_fields'],
                'table_fields': self.model['table_fields'],
                'sort_fields': self.model['sort_fields'],
                'filter_fields': self.model['filter_fields'],
                'id_key': self.model['id_key'],
                'info_fields': self.model['info_fields'],
                'all_fields': self.model['all_fields'],
                'math': math,
                'labels': self.model['labels'],
                'inv_labels': {v: k for k, v in self.model['labels'].items()},
                'facet_url': facet_url,
                'edit_url': edit_url,
                'base_url': base_url,
                'sort_url': sort_url,
                'sort_arrow': sort_arrow,
                'driller_link': driller_link,
                'selected_facets': selected_facets,
                'exclude_facet': exclude_facet,
                'get_hidden': get_hidden,
                'make_link': make_link,
                'geo_locality': None,
                'batch_size': batch_size,
                'javascript_start': '',
                'javascript_middle': '',
                'javascript_end': '',
                'main_template': main_template,
                'augmentations': [],
                'detailed_augmentations': {},
                'specimen_augmentation': '',
                'batched_augmentations': {},
                'driller': self.model.get('driller', ''),
                'tab_link': tab_link,
                'nearby_link': nearby_link,
                'action_link': action_link,
                'id_url': id_url,
                'chunks': chunks,
                'likesolr': self,
                'search_info': search_info,
                'autocomplete_path': autocomplete_path,
                'get_search_input': get_search_input,
                'query': self.query}

    def index(self):
        self.result = self.get_result()
        self.prepare()
        self.doc = self.doc_results(self.get_doc())
        lines = self.facet_results(self.get_facets())
        self.docs_results(lines)
        if self.query.params.get('tab', None) == 'dashboard':
            if len(self.query.dims) <= 1:
                simple_facet_count = self.get_simple_facet_count(self.facet_results(self.get_facets()))
                if simple_facet_count is not None:
                    self.simple_facet_count_results(simple_facet_count)
            else:
                driller = self.get_driller()
                if driller is not None:
                    self.driller_results(driller, lines, self.result['labels'])
        for augmentation in self.augmentations:
            if self.query.params.get('context', None) == 'specimen':
                number = 1
            else:
                if 'table' in self.result:
                    number = len(self.result['table']['response']['docs'])
                else:
                    number = 1
            if number == 1 and 'geohash' not in self.request.params:
                if augmentation.is_applicable(self):
                    augmentation.prefetch(self)
                    self.result['detailed_augmentations'][augmentation.name] = augmentation
                    if self.query.params.get('tab', 'info') == augmentation.name:
                        self.result['specimen_augmentation'] = augmentation.callback(self)
            else:
                if augmentation.is_batched_applicable(self):
                    self.result['batched_augmentations'][augmentation.name] = augmentation.batch_callback(self)

        return self.result

    def prepare(self):
        search_term = self.query.params.get('search_term', '')
        items = []
        if search_term:
            for item in search_term.split():
                items.append("*" + item + "*")
            self.search_value = quote(" AND ".join(items))
            self.search = '&q=' + self.search_value
        else:
            self.search_value = '*'
            self.search = '&q=' + self.search_value

        self.result['search_term'] = search_term

        self.geo_part_dict = MultiDict()
        self.geo_part = ""
        if 'geohash' in self.query.params:
            box = bbox(self.query.params['geohash'])
            rectangle = "[%s,%s TO %s,%s]" % (str(box['s']), str(box['w']), str(box['n']), str(box['e']))
            self.geo_part_dict['fq'] = 'geo_locality:' + quote(rectangle.encode('utf8'))

        geo_locality = self.query.fqs.get('geo_locality', None)
        if geo_locality is None and 'd' in self.query.params:
            geo_locality = '"%s"' % self.result['doc']['response']['docs'][0]['geo_locality']  # nopep8
        if geo_locality is not None:
            if 'd' in self.query.params:
                dist = self.query.params['d']
                self.geo_part = "&pt=%s" % geo_locality[1:-1] + "&d=%s" % dist[0] + '&fq={!geofilt%20sfield=geo_locality}'  # nopep8
                self.geo_part_dict['pt'] = geo_locality[1:-1]
                self.geo_part_dict['d'] = dist
                self.geo_part_dict['fq'] = '{!geofilt%20sfield=geo_locality}'
            else:
                self.geo_part_dict['fq'] = 'geo_locality:%22Contains(' + geo_locality[1:-1] + ')%22'  # nopep8
            self.result['geo_locality'] = geo_locality[1:-1]

            self.result['javascript'] = []
            self.result['javascript_start'] += """
// base prepare - geo_locality: %s""" % geo_locality
            self.result['javascript_start'] += """
// base prepare
function initialize() {

        var center = new google.maps.LatLng(%s);

        var map = new google.maps.Map(document.getElementById('map-canvas'), {
          zoom: 8,
          center: center,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });

    var markers = [];
    for (var i = 0; i < locations.length; i++) {
      var location = locations[i];
      var latLng = new google.maps.LatLng(location[0], location[1]);
      var marker = new google.maps.Marker({'position': latLng});
      markers.push(marker);
    }
    var markerCluster = new MarkerClusterer(map, markers);
}
""" % self.result['geo_locality']
            self.result['javascript_middle'] = """
// base prepare"""
            self.result['javascript_end'] = """
// base prepare
google.maps.event.addDomListener(window, 'load', initialize);"""

    def get_batch(self, num_found, batch_size):
        start = 0
        if 'start' in self.query.params:
            start = int(self.query.params.get('start'))
            if start == 0:
                pass
            else:
                start = start - 1
        orphan = 10
        if not (orphan < batch_size):
            orphan = max(batch_size - 1, 0)
        return Batch(xrange(num_found), size=batch_size, start=start * batch_size, orphan=orphan)  # nopep8
